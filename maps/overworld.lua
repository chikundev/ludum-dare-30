return {
  version = "1.1",
  luaversion = "5.1",
  orientation = "orthogonal",
  width = 64,
  height = 64,
  tilewidth = 16,
  tileheight = 16,
  properties = {},
  tilesets = {
    {
      name = "overworldBackground",
      firstgid = 1,
      tilewidth = 1024,
      tileheight = 1024,
      spacing = 0,
      margin = 0,
      image = "../gfx/backgrounds/overworldBackground.jpg",
      imagewidth = 1024,
      imageheight = 1024,
      properties = {},
      tiles = {}
    },
    {
      name = "dustPlanet",
      firstgid = 2,
      tilewidth = 128,
      tileheight = 128,
      spacing = 0,
      margin = 0,
      image = "../gfx/planetsOverworld/dustPlanet.png",
      imagewidth = 128,
      imageheight = 128,
      properties = {},
      tiles = {}
    },
    {
      name = "firePlanet",
      firstgid = 3,
      tilewidth = 128,
      tileheight = 128,
      spacing = 0,
      margin = 0,
      image = "../gfx/planetsOverworld/firePlanet.png",
      imagewidth = 128,
      imageheight = 128,
      properties = {},
      tiles = {}
    },
    {
      name = "grassPlanet",
      firstgid = 4,
      tilewidth = 128,
      tileheight = 128,
      spacing = 0,
      margin = 0,
      image = "../gfx/planetsOverworld/grassPlanet.png",
      imagewidth = 128,
      imageheight = 128,
      properties = {},
      tiles = {}
    },
    {
      name = "waterPlanet2",
      firstgid = 5,
      tilewidth = 128,
      tileheight = 128,
      spacing = 0,
      margin = 0,
      image = "../gfx/planetsOverworld/waterPlanet2.png",
      imagewidth = 128,
      imageheight = 128,
      properties = {},
      tiles = {}
    },
    {
      name = "airPlanet",
      firstgid = 6,
      tilewidth = 128,
      tileheight = 128,
      spacing = 0,
      margin = 0,
      image = "../gfx/planetsOverworld/airPlanet.png",
      imagewidth = 128,
      imageheight = 128,
      properties = {},
      tiles = {}
    }
  },
  layers = {
    {
      type = "objectgroup",
      name = "bg",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 0,
          y = 1024,
          width = 0,
          height = 0,
          gid = 1,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "important",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          name = "playerStart",
          type = "",
          shape = "rectangle",
          x = 400,
          y = 496,
          width = 48,
          height = 48,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "collisions",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 0,
          y = 0,
          width = 16,
          height = 1024,
          visible = true,
          properties = {}
        },
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 16,
          y = 1008,
          width = 1008,
          height = 16,
          visible = true,
          properties = {}
        },
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 1008,
          y = 0,
          width = 16,
          height = 1008,
          visible = true,
          properties = {}
        },
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 16,
          y = 0,
          width = 992,
          height = 16,
          visible = true,
          properties = {}
        }
      }
    }
  }
}
