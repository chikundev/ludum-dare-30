-- chikun :: 2014
-- Template text file


return {
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "",
        text    = "As you climb into the exosphere, a rush of energy fills you with purpose."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "",
        text    = "You must realise your destiny as a child of the Elder Turtle."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "",
        text    = "Perhaps even to become the Elder Turtle."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "",
        text    = "But to do so, you must find your brethren, and conquer your fears."
    },
}
