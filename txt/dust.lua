return {
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "",
        text    = "Even in the confines of your protective helmet, it feels like you are close to choking on the dry, humid air of this dusty, arid world."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "",
        text    = "Every step your flipper takes kicks up dust from the ground, which sends a thick plume into the air."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "",
        text    = "How can anything manage to live here?"
    },
}
