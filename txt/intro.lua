-- chikun :: 2014
-- Intro text scene


return {
    {   -- Change bg colour
        type    = "function",
        create  = function(dt)
            g.setBackgroundColor(0, 0, 0)
            chat.progress()
    world.opened = {
            water   = false,
            fire    = false,
            dust    = false,
            air     = false,
            grass   = false
        }
        end,
        update  = function(dt)

        end
    },
    {
        type    = "text",
        speaker = "",
        text    = "For years, our kind have yearned to venture beyond the stars."
    },
    {
        type    = "text",
        speaker = "",
        text    = "To realize a preordained destiny, or to fulfil the aching call of wanderlust."
    },
    {
        type    = "text",
        speaker = "",
        text    = "As the seas warmed, and engulfed the land, there was nothing left to call home, and many were prepared to take to the skies and leave all they had called home behind."
    },
    {
        type    = "text",
        speaker = "",
        text    = "Now, you are the last of your kind. You are the last turtle, left behind."
    },
    {
        type    = "text",
        speaker = "",
        text    = "Where have your kin gone?"
    },
    {
        type    = "text",
        speaker = "",
        text    = "Somewhere in the midst of the planets and stars, you hope to find them once more..."
    },
    {
        type    = "question",
        speaker = "",
        text    = "Would you like to venture out?",
        onRes   = function(result)
            if result then
                bgm.intro:stop()
                state.current = states.play
                fadeStart(maps.water.hub)
            else
                chat.progress()
            end
        end
    },
    {
        type    = "text",
        speaker = "",
        text    = "Fine, we didn't want you in the cosmos anyway."
    },
    {
        type    = "function",
        create  = function()
            bgm.intro:stop()
            state.change(states.menu)
        end
    }
}
