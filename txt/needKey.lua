-- chikun :: 2014
-- Crab key text file


return {
    {   -- This will make text appear at the bottom of the screen
        type    = "crabDown",
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "THIS DOOR REQUIRES A KEY."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "crabUp",
    },
}
