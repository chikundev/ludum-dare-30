-- chikun :: 2014
-- Water boss text file


return {
    {   -- This moves the viewport to a specific point at a speed
        type    = "function",
        create  = function()
            bgm.waterBossLoop:stop()
            chat.progress()
        end,
        update  = function(dt)

        end
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "",
        text    = "Having defeated the blue knight, the turtle boards his trusty spaceship."
    },
    {   -- This moves the viewport to a specific point at a speed
        type    = "function",
        create  = function()
            world.opened.water = true
            state.current = states.play
            overworldShow = true
            fadeStart(maps.overworld)
        end,
        update  = function(dt)

        end
    },
}
