-- chikun :: 2014
-- Template text file


-- Useful code for question + function modes:
-- chat.progress() -> progresses you to next chat
-- state.current = states.play -> leaves chat
-- chat.start(txt.template) -> starts txt.template


return {
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "Chris",
        text    = "description"
    },
    {   -- This moves the viewport to a specific point at a speed
        type    = "shift",
        toX     = 600,
        toY     = 0,
        speed   = 512
    },
    {   -- This performs some code
        type    = "function",
        create  = function(dt)
            -- Function to be executed on creation
        end,
        update  = function(dt)
            -- Function to be executed each update loop
        end
    },
    {   -- This asks the player a yes/no question
        type    = "question",
        speaker = "",
        text    = "Would you like to venture out?",
        onRes   = function(result)
            -- If result is yes
            if result then
                -- do stuff
            else
                -- do other stuff
            end
        end
    },
}
