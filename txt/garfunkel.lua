-- chikun :: 2014
-- Garfunkel text file

local action, menu = ""
if s.getOS() == "Android" then
    action = "A BUTTON"
    menu = "Menu"
else
    action = "SPACE BAR"
    menu = "I or Escape"
end

return {
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "",
        text    = "Your lifelong friend and mentor, Art Garfunkel, is sitting contentedly in a garden."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "Art Garfunkel",
        text    = "Hello little turtle."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "Art Garfunkel",
        text    = "I see you are finally ready to leave. After all these years, it is time."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "Art Garfunkel",
        text    = "You are ready to uncover the true powers of the Elder Turtle."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "Art Garfunkel",
        text    = "I can show you the path to the Elder Turtle, but it is a journey you must take alone."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "Art Garfunkel",
        text    = "As you conquer the stars in search for your kind, you will grow stronger."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "Art Garfunkel",
        text    = "Let me show you the way, little turtle. I will teach you."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "",
        text    = "You have learned Water Shot!"
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "crabDown",
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "I AM TUTORIAL CRAB!"
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "LET ME HELP YOU LEARN THE WAYS OF THE ELDER TURTLE!"
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "YES, I WILL COME WITH YOU! CRAB AND TURTLE! WE WILL CONQUER THE GALAXY!"
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "ANYWAY... YOU LEARNT A SKILL, RIGHT?"
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "YOU CAN SHOOT WATER OUT OF YOUR LITTLE TURTLE MOUTH BY USING THE " .. action .. ", YES?"
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "WHEN SHOT, YOU WILL UNLEASH A WAVE OF TERROR AND DESTRUCTION UPON YOUR ENEMIES!"
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "HOW DOES IT WORK? I DON'T KNOW, I'M JUST A CRAB."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "crabUp",
    },
    {
        type    = "function",
        create  = function()
            power.waterShot = true
            selected = 1
            chat.progress()
        end,
        update  = function(dt)

        end
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "",
        text    = "You can access your inventory by pressing " .. menu .. "."
    },
}
