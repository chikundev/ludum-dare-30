-- chikun :: 2014
-- Hell boss text file


return {
    {   -- This moves the viewport to a specific point at a speed
        type    = "shift",
        toX     = 192,
        toY     = 644,
        speed   = 20000000
    },
    {   -- This moves the viewport to a specific point at a speed
        type    = "function",
        create  = function()
            bgm.hellWorld:stop()
            chat.progress()
        end,
        update  = function(dt)

        end
    },
    {   -- This moves the viewport to a specific point at a speed
        type    = "shift",
        toX     = 192,
        toY     = 96,
        speed   = 512
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "Broodmother",
        text    = "You have come this far to search for your kind, only, can I say, to be sorely disappointed."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "Broodmother",
        text    = "When the Elder Turtles took to the skies, they never quite found a new place to call home..."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "Broodmother",
        text    = "Yes, now you see what I mean. For I am the last of the Elder Turtles, and your are the last of the turtles."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "Broodmother",
        text    = "The rest linger here in the Underworld, a sore memory of a tortured past."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "Broodmother",
        text    = "I was once destroyed by a little turtle of the likes of you."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "Broodmother",
        text    = "He, too, came to search for things he did not know would ruin him. And here you are."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "Broodmother",
        text    = "Perhaps you want to become the Elder Turtle. There is only one way."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "Broodmother",
        text    = "You are going to have to defeat me. And then you truly will be the last of the Elder Turtles."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "Broodmother",
        text    = "Are you ready to give me everything you've got?"
    },
    {   -- This moves the viewport to a specific point at a speed
        type    = "shift",
        toX     = 192,
        toY     = 664,
        speed   = 512
    },
    {   -- This moves the viewport to a specific point at a speed
        type    = "function",
        create  = function()
            bgm.hellBoss:play()
            chat.progress()
        end,
        update  = function(dt)

        end
    },
}
