-- chikun :: 2014
-- Grass boss text file

local action = ""
if s.getOS() == "Android" then
    action = "A BUTTON"
else
    action = "SPACE BAR"
end

return {
    {   -- This moves the viewport to a specific point at a speed
        type    = "function",
        create  = function()
            bgm.grassBossLoop:stop()
            chat.progress()
        end,
        update  = function(dt)

        end
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "",
        text    = "Having defeated the green knight, the turtle obtains a new ability."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "crabDown",
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "I THINK YOU PRESS THE " .. action .. " TO USE THIS ABILITY, OK?"
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "WHEN YOU USE IT, YOU STOP YOUR ADVERSARY RIGHT IN THEIR TRACKS!"
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "YOU SUMMON THE SPIRIT OF THE ELDER TURTLE TO DRIVE YOUR OPPONENT WILD!"
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "ACTUALLY, I THINK YOU PROBABLY TURN THEM BRAINDEAD. THAT'S PRETTY CALLOUS."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "COME TO THINK OF IT, THIS SKILL IS HORRIFYING."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "I THINK I MIGHT GO BEFORE YOU USE IT ON SOMEONE."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "crabUp",
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "",
        text    = "The turtle returns to space."
    },
    {   -- This moves the viewport to a specific point at a speed
        type    = "function",
        create  = function()
            world.opened.grass = true
            power.wildNature = true
            selected = 4
            state.current = states.play
            fadeStart(maps.overworld)
        end,
        update  = function(dt)

        end
    },
}
