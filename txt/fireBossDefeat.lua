-- chikun :: 2014
-- Fire boss text file

local action = ""
if s.getOS() == "Android" then
    action = "A BUTTON"
else
    action = "SPACE BAR"
end

return {
    {   -- This moves the viewport to a specific point at a speed
        type    = "function",
        create  = function()
            bgm.fireBossLoop:stop()
            chat.progress()
        end,
        update  = function(dt)

        end
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "",
        text    = "Having defeated the red knight, the turtle obtains a new ability."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "crabDown",
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "I GUESS YOU PRESS THE " .. action .. " TO CAST EVERYTHING AFLAME, RIGHT?"
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "THIS ONE LOOKS A LITTLE DANGEROUS..."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "I DON'T WANT TO BE BURNED TO A CRISP."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "PLEASE KEEP A FEW FEET AWAY FROM ME FROM NOW ON WHEN WE TALK, OK?"
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "crabUp",
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "",
        text    = "The turtle returns to space."
    },
    {   -- This moves the viewport to a specific point at a speed
        type    = "function",
        create  = function()
            world.opened.fire = true
            power.fireBreath = true
            selected = 1
            state.current = states.play
            fadeStart(maps.overworld)
        end,
        update  = function(dt)

        end
    },
}
