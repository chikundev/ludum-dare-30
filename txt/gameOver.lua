-- chikun :: 2014
-- Game over text scene


return {
    {   -- Change bg colour
        type    = "function",
        create  = function(dt)
            g.setBackgroundColor(0, 0, 0)
            a.stop()
            chat.progress()
        end,
        update  = function(dt)

        end
    },
    {
        type    = "text",
        speaker = "",
        text    = "I'm sorry, you couldn't make it. You have lost the turtle."
    },
    {
        type    = "question",
        speaker = "",
        text    = "Unfortunately, graves cannot exist in space. Would you like to try again?",
        onRes   = function(result)
            if result then
                bgm.intro:stop()
                fadeStart(maps[world.current].hub)
                gameIsOver = false
            else
                chat.progress()
            end
        end
    },
    {
        type    = "text",
        speaker = "",
        text    = "Fine, we didn't want you in the cosmos anyway."
    },
    {
        type    = "function",
        create  = function()
            state.change(states.menu)
            hidePlayer = nil
            gameIsOver = false
        end
    }
}
