-- chikun :: 2014
-- Air boss text file

local action = ""
if s.getOS() == "Android" then
    action = "A BUTTON"
else
    action = "SPACE BAR"
end

return {
    {   -- This moves the viewport to a specific point at a speed
        type    = "function",
        create  = function()
            bgm.airBossLoop:stop()
            chat.progress()
        end,
        update  = function(dt)

        end
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "",
        text    = "Having defeated Michael Jordan, the turtle obtains a new ability."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "crabDown",
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "DAMN! PRESS " .. action .. " TO PERFORM A REVERSE SLAM DUNK!"
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "IT LITERALLY AND VIOLENTLY BLOWS THE COMPETITION OUT OF THE WATER!"
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "MICHAEL JORDAN'S GOT NOTHING ON YOUR REVERSE DUNK TECHNIQUE!"
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "WE SHOULD SHOOT HOOPS SOMETIME DOWN AT THE COURT."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "crabUp",
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "",
        text    = "The turtle returns to space."
    },
    {   -- This moves the viewport to a specific point at a speed
        type    = "function",
        create  = function()
            world.opened.air = true
            power.airShove = true
            selected = 3
            state.current = states.play
            fadeStart(maps.overworld)
        end,
        update  = function(dt)

        end
    },
}
