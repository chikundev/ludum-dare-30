-- chikun :: 2014
-- Grass boss text file


return {
    {   -- This moves the viewport to a specific point at a speed
        type    = "shift",
        toX     = 192,
        toY     = 644,
        speed   = 20000000
    },
    {   -- This moves the viewport to a specific point at a speed
        type    = "shift",
        toX     = 192,
        toY     = 96,
        speed   = 512
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "Green Knight",
        text    = "Get ready to have your grass annihilated!"
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "crabDown",
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "I THOUGHT YOU HIPPIES WERE SUPPOSED TO BE FRIENDLY."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "crabUp",
    },
    {   -- This moves the viewport to a specific point at a speed
        type    = "shift",
        toX     = 192,
        toY     = 664,
        speed   = 512
    },
    {   -- This moves the viewport to a specific point at a speed
        type    = "function",
        create  = function()
            bgm.grassBossIntro:stop()
            bgm.grassBossLoop:play()
            chat.progress()
        end,
        update  = function(dt)

        end
    },
}
