return {
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "",
        text    = "A cool temperate breeze passes as you touch down on this world."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "",
        text    = "You have never seen land this lush before, since the seas engulfed your home planet's surface many thousands of years before."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "",
        text    = "Everything is so green."
    },
}
