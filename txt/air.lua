return {
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "",
        text    = "Basketball."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "",
        text    = "You never played it on your home planet, but occasionally you saw some taped NBA re-runs on the sports network and it looked exciting."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "",
        text    = "Can a turtle dribble down the court? Can a turtle manage to pull off a slam dunk? There's only one way to find out."
    },
}
