-- chikun :: 2014
-- Water boss text file


return {
    {   -- This moves the viewport to a specific point at a speed
        type    = "shift",
        toX     = 192,
        toY     = 644,
        speed   = 20000000
    },
    {   -- This moves the viewport to a specific point at a speed
        type    = "shift",
        toX     = 192,
        toY     = 96,
        speed   = 512
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "Blue Knight",
        text    = "I'll send you to the depths of the ocean!"
    },
    {   -- This moves the viewport to a specific point at a speed
        type    = "crabDown",
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "THE DOOR IS CLOSED, WE ARE TRAPPED HERE WITH YOU."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "Blue Knight",
        text    = "Then we must fight!"
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "VERY WELL."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "crabUp",
    },
    {   -- This moves the viewport to a specific point at a speed
        type    = "shift",
        toX     = 192,
        toY     = 664,
        speed   = 512
    },
    {   -- This moves the viewport to a specific point at a speed
        type    = "function",
        create  = function()
            bgm.waterBossIntro:stop()
            bgm.waterBossLoop:play()
            chat.progress()
        end,
        update  = function(dt)

        end
    },
}
