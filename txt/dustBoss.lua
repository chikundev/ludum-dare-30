-- chikun :: 2014
-- Dust boss text file


return {
    {   -- This moves the viewport to a specific point at a speed
        type    = "shift",
        toX     = 192,
        toY     = 644,
        speed   = 20000000
    },
    {   -- This moves the viewport to a specific point at a speed
        type    = "shift",
        toX     = 192,
        toY     = 96,
        speed   = 512
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "Dust Knight",
        text    = "I'll turn you into dust!"
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "",
        text    = "..."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "",
        text    = "..."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "I'M SCARED."
    },
    {   -- This moves the viewport to a specific point at a speed
        type    = "shift",
        toX     = 192,
        toY     = 664,
        speed   = 512
    },
    {   -- This moves the viewport to a specific point at a speed
        type    = "function",
        create  = function()
            bgm.dustBossIntro:stop()
            bgm.dustBossLoop:play()
            chat.progress()
        end,
        update  = function(dt)

        end
    },
}
