-- chikun :: 2014
-- Fire boss text file


return {
    {   -- This moves the viewport to a specific point at a speed
        type    = "shift",
        toX     = 192,
        toY     = 644,
        speed   = 20000000
    },
    {   -- This moves the viewport to a specific point at a speed
        type    = "shift",
        toX     = 192,
        toY     = 96,
        speed   = 512
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "Red Knight",
        text    = "You'll leave this place as a pile of ashes!"
    },
    {   -- This moves the viewport to a specific point at a speed
        type    = "crabDown",
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "WE WERE SENT HERE TO DESTROY YOU."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "Blue Knight",
        text    = "Then we must fight!"
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "YEAH OK THANKS."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "crabUp",
    },
    {   -- This moves the viewport to a specific point at a speed
        type    = "shift",
        toX     = 192,
        toY     = 664,
        speed   = 512
    },
    {   -- This moves the viewport to a specific point at a speed
        type    = "function",
        create  = function()
            bgm.fireBossIntro:stop()
            bgm.fireBossLoop:play()
            chat.progress()
        end,
        update  = function(dt)

        end
    },
}
