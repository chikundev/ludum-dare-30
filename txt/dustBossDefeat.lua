-- chikun :: 2014
-- Dust boss text file

local action = ""
if s.getOS() == "Android" then
    action = "A BUTTON"
else
    action = "LEFT CONTROL"
end

return {
    {   -- This moves the viewport to a specific point at a speed
        type    = "function",
        create  = function()
            bgm.dustBossLoop:stop()
            chat.progress()
        end,
        update  = function(dt)

        end
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "",
        text    = "Having defeated the dust knight, the turtle obtains a new ability."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "crabDown",
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "NEAT! PRESS " .. action .. " TO SPIN IN A DUST CLOUD INCREDIBLY FAST!"
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "I'M NO SCIENTIST, BUT I'M SURE EVERYBODY HATES DUST!"
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "USE THIS TO YOUR ADVANTAGE! KICK THAT DUST IN THEIR EYES!"
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "MAKE SURE TO KEEP SOME DUST IN YOUR POCKET FOR EASY ACCESS."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "crabUp",
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "",
        text    = "The turtle returns to space."
    },
    {   -- This moves the viewport to a specific point at a speed
        type    = "function",
        create  = function()
            world.opened.dust = true
            power.defence = true
            state.current = states.play
            fadeStart(maps.overworld)
        end,
        update  = function(dt)

        end
    },
}
