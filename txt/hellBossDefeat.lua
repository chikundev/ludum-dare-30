-- chikun :: 2014
-- Hell boss text file

local action = ""
if s.getOS() == "Android" then
    action = "A BUTTON"
else
    action = "SPACE BAR"
end

return {
    {   -- This moves the viewport to a specific point at a speed
        type    = "function",
        create  = function()
            a.stop()
            endGame = true
            chat.progress()
        end,
        update  = function(dt)

        end
    },
    {   -- This moves the viewport to a specific point at a speed
        type    = "function",
        create  = function()
            love.timer.sleep(1)
            chat.progress()
        end,
        update  = function(dt)

        end
    },
    {
        type = "crabDown"
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "HAVING DEFEATED THE BROODMOTHER AND ALLAYING THE FORCES OF THE UNDERWORLD, YOU HAVE PROVEN YOURSELF A QUITE CAPABLE TURTLE."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "PERHAPS THE BEST TURTLE TO EVER ROAM THE CORNERS OF THE GALAXY."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "YOU HAVE PROVEN YOURSELF TO BE MORE THAN A 'LITTLE TURTLE'."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "YOUR STRENGTH AND SKILL ARE UNMATCHED."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "YOU HAVE BECOME... AN ELDER TURTLE. THE LAST ELDER TURTLE."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "BUT THE JOURNEY HAS JUST BEGUN."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "crabUp",
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "",
        text    = "The turtle returns to space."
    },
    {   -- This moves the viewport to a specific point at a speed
        type    = "function",
        create  = function()
            state.change(states.credits)
        end,
        update  = function(dt)

        end
    },
}
