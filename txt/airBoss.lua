-- chikun :: 2014
-- Air boss text file


return {
    {   -- This moves the viewport to a specific point at a speed
        type    = "shift",
        toX     = 192,
        toY     = 644,
        speed   = 20000000
    },
    {   -- This moves the viewport to a specific point at a speed
        type    = "shift",
        toX     = 192,
        toY     = 96,
        speed   = 512
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "Michael Jordan",
        text    = "There is no hoop here, turn back!"
    },
    {   -- This moves the viewport to a specific point at a speed
        type    = "crabDown",
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "HOLY FUCK YOU'RE MICHAEL JORDAN."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "Michael Jordan",
        text    = "You dare step into the court of the Dunk Master?"
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "TUTORIAL CRAB",
        text    = "CAN YOU SIGN MY PINCER?"
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "Michael Jordan",
        text    = "No."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "crabUp",
    },
    {   -- This moves the viewport to a specific point at a speed
        type    = "shift",
        toX     = 192,
        toY     = 664,
        speed   = 512
    },
    {   -- This moves the viewport to a specific point at a speed
        type    = "function",
        create  = function()
            bgm.airBossIntro:stop()
            bgm.airBossLoop:play()
            chat.progress()
        end,
        update  = function(dt)

        end
    },
}
