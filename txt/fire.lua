return {
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "",
        text    = "Embers and charred surfaces spit smoke under your flippers. It's incredibly hot."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "",
        text    = "Sweat beads dangle of your scaly little cheeks as you ponder the sweltering oven of a planet that looms before you."
    },
}
