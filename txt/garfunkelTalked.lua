-- chikun :: 2014
-- Garfunkel talked text file


return {
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "Art Garfunkel",
        text    = "I see the fire of determination in your eyes..."
    },
    {   -- This will make text appear at the bottom of the screen
        type    = "text",
        speaker = "Art Garfunkel",
        text    = "As you set out to take to the stars, I hope you find what you seek."
    },
}
