-- chikun :: 2014
-- Configuration file


function love.conf(game)

    -- Attach a console for Windows debug [DISABLE ON DISTRIBUTION]
    game.console    = false

    -- Version of LÖVE which this game was made for
    game.version    = "0.9.1"

    -- Omit modules due to disuse
    game.modules.math   = false
    game.modules.thread = false

    -- Various window settings

    game.window.title   = "Star Turtle 64"
    game.window.width   = 1280
    game.window.height  = 720

    -- Scaling options
    game.window.resizable   = true
    game.window.minwidth    = 640
    game.window.minheight   = 360

end
