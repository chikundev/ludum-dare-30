-- chikun :: 2014
-- Tiled .lua map loading tool


-- Table containing map functions
map = {
    current = nil
}


-- Loads and returns a map
function map.load(mapName)

    -- Load map into a table
    local mapData = require("maps/" .. mapName)

    -- New map structure
    local newMap = {
        layers    = { },
        quads     = { },
        tilesets  = { },
        w       = mapData.width,
        h       = mapData.height,
        tileW   = mapData.tilewidth,
        tileH   = mapData.tileheight,
        name    = mapName
    }

    -- Loads all quads from the map data
    for key, extTileset in ipairs(mapData.tilesets) do
        -- Our new tileset <3
        local n = { }

        -- Interpret tileset's image into an actual loaded image
        n.image = getResourceFromString(extTileset.image)

        -- Transfer certain values to the new tileset
        n.gid     = extTileset.firstgid
        n.imageW  = extTileset.imagewidth
        n.imageH  = extTileset.imageheight
        n.margin  = extTileset.margin
        n.spacing = extTileset.spacing
        n.tileW   = extTileset.tilewidth
        n.tileH   = extTileset.tileheight

        -- Determine quad value range
        local repeatX = math.ceil((n.imageW - n.spacing) / n.tileW) - 1
        local repeatY = math.ceil((n.imageH - n.spacing) / n.tileH) - 1
        local yPos    = n.margin

        -- Start counting at current gid
        local gidQ = n.gid

        -- Interpret quads from the tileset
        for currentY = 0, repeatY do
            -- Reset x starting position
            local xPos = n.margin

            for currentX = 0, repeatX do
                -- Add current quad to the table
                newMap.quads[gidQ] =
                    g.newQuad(xPos + (currentX * n.tileW), yPos + (currentY * n.tileH),
                    n.tileW, n.tileH, n.imageW, n.imageH)

                -- Increment x starting position
                xPos = xPos + n.spacing

                -- Increment quad number
                gidQ = gidQ + 1
            end

            -- Increment y starting position
            yPos = yPos + n.spacing
        end

        -- Add to the list of map tilesets
        table.insert(newMap.tilesets, n)
    end


    -- Load layers into tables
    for key, layer in ipairs(mapData.layers) do
        -- Create new table based on layer name
        local newLayer   = {
            data    = layer.data,
            name    = layer.name,
            type    = layer.type,
            vars    = layer.properties,
            w       = layer.width,
            h       = layer.height
        }

        -- Create table for objects
        local newObjects = { }

        -- If layer isn't tilelayer, import objects
        if layer.type ~= "tilelayer" then
            for key, object in ipairs(layer.objects) do
                -- Get data from object's quad, if available
                local quadVars = { }
                if object.gid then
                    quadVars = {newMap.quads[object.gid]:getViewport()}
                end

                -- Add object to table
                newObjects[#newObjects+1] = {
                    name = object.name,
                    type = object.type,
                    gid = object.gid,
                    w   = (quadVars[3] or object.width),
                    h   = (quadVars[4] or object.height),
                    x   = object.x,
                    y   = object.y - (quadVars[4] or 0)
                }
            end
        end

        -- Add new layer to mapData
        newLayer.objects = newObjects
        table.insert(newMap.layers, newLayer)
    end
    -- Return the newly created map
    return newMap

end


-- Draws the current or given map
function map.draw(givenMap)

    -- If map isn't given, default to current
    local u = givenMap or map.current

    -- Draw all layers
    for key, layer in ipairs(u.layers) do

        -- If tile layer...
        if layer.type == "tilelayer" then
            -- ...then draw all tiles in layer
            for key, tile in ipairs(layer.data) do

                -- Calculated placement of tile
                local tmpY = math.floor((key - 1) / u.w)
                local tmpX = key - (tmpY * u.w) - 1

                -- If tile exists, draw it
                if tile > 0 then
                    local quadVars = {u.quads[tile]:getViewport()}
                    g.draw(getImageFromGID(tile), u.quads[tile], tmpX * u.tileW, tmpY * u.tileH - quadVars[4] + u.tileH)
                end

            end
        -- If swap layer...
        --[[elseif layer.name == "levelSwap" then
            -- ...then draw placeholder gfx
            swaps = layer.objects
            g.setColor(100, 50, 125)
            for key, swap in ipairs(swaps) do
                if swap.name == "door" then
                    g.rectangle("fill", swap.x, swap.y, swap.width, swap.height, 0)
                end
            end]]--
        -- Otherwise...
        else
            -- ...draw all objects in layer...
            for key, object in ipairs(layer.objects) do
                -- ...if that's possible
                if object.gid then
                    g.draw(getImageFromGID(object.gid), u.quads[object.gid], object.x, object.y)
                end
            end
        end

    end

end


-- Change the map in a meaningful way, Chris-senpai
function map.change(nextMap, portal)

    -- Load first map
    map.current = nextMap

    setTheme(nextMap.name)

    world.currentMap = nextMap.name

    -- Delete all enemies
    enemies = { }
    ene.projectile = { }
    player.projectile = { }
    swaps = { }
    enemySpawns = { }
    collisions = { }
    people = { }

    -- Go through layers in map
    for key, layer in ipairs(map.current.layers) do

        -- Interpret important layer
        if layer.name == "important" then
            for key, object in ipairs(layer.objects) do
                -- Place player at start
                if object.name == "playerStart" and portal == nil then
                    checkpoint = object
                end
            end
        -- Move collisions to their own layer
        elseif layer.name == "collisions" then
            collisions = layer.objects
        elseif layer.name == "levelSwap" then
            swaps = layer.objects

            for key, swap in ipairs(swaps) do
                if swap.name == portal then
                    checkpoint = {
                        x = swap.x + swap.w/2 - 12,
                        y = swap.y + swap.h/2 - 12,
                        w = 24,
                        h = 24
                    }
                end
            end
        elseif layer.name == "enemies" then
            enemySpawns = layer.objects
            enemy.spawn()
            for k, v in ipairs(enemySpawns) do
                if v.name == "bossSpawn" then
                    chat.start(txt[world.current .. "Boss"])
                end
            end
        elseif layer.name == "people" then
            people = layer.objects
            for k, v in ipairs(people) do
                table.insert(collisions, v)
            end
        end
    end

    -- Reset view
    resetView()

    -- Set player to checkpoint start
    player:create(checkpoint.x, checkpoint.y, checkpoint.w, checkpoint.h)

    -- Get map size
    mapW = map.current.w * map.current.tileW
    mapH = map.current.h * map.current.tileH


    -- Create specific map thangzz
    specific.create(world.currentMap)

end


-- Returns the image used by a certain gid
function getImageFromGID(gid, givenMap)

    -- If map isn't given, default to current
    local u = givenMap or map.current

    -- Count current number of tilesets
    local num = #u.tilesets

    -- Seek backwards through tilesets until desired image found
    while (gid < u.tilesets[num].gid) do
        num = num - 1
    end

    -- Return found image
    return u.tilesets[num].image

end


function setTheme(name)

    local old = {getSongFromName(world.currentMap)}
    local new = {getSongFromName(name)}

    if old[1] ~= new[1] and old[1] ~= "" then
        print(old[1])
        bgm[old[1]]:stop()
    end
    if new[1] ~= "" then
        bgm[new[1]]:play()
    end

    world.current = new[2]

end


function getSongFromName(name)

    local bg, wor = "", world.current

    if name:find("grass") == 1 then
        bg = "grass"
        wor = "grass"
    elseif name:find("fire") == 1 then
        bg = "fire"
        wor = "fire"
    elseif name:find("water") == 1 then
        bg = "water"
        wor = "water"
    elseif name:find("air") == 1 then
        bg = "air"
        wor = "air"
    elseif name:find("dust") == 1 then
        bg = "dust"
        wor = "dust"
    end

    if bg ~= "" then
        if name:find("Temple") or name:find("Cave") then
            bg = bg .. "Underworld"
        elseif name:find("Boss") then
            bg = bg .. "BossIntro"
        else
            bg = bg .. "Overworld"
        end
    end

    if name:find("hell") == 1 then
        bg = "hellWorld"
        wor = "hell"
    end

    if name == "overworld" then
        bg = "space"
        wor = "overworld"
    end

    if playIntro then
        bg = "intro"
        wor = "overworld"
    end

    return bg, wor

end
