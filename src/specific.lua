-- chikun :: 2014
-- Performs specific functions based on map


-- Creates table for specific functions
specific = { }


-- On map creation
function specific.create(cMap)
    if cMap == "overworld" then
        local distance = 384
        local sin = math.sin
        local cos = math.cos

        local angles = {
            dust = math.rad(270),
            fire = math.rad(342),
            air = math.rad(54),
            water = math.rad(126),
            grass = math.rad(198)
        }

        planets = {
            dust = {
                x = distance * cos(angles.dust) + 512 - 64,
                y = distance * sin(angles.dust) + 512 - 64,
                img = gfx.planetsOverworld.dustPlanet,
                glow = gfx.planetsOverworld.dustGlow
            },
            fire = {
                x = distance * cos(angles.fire) + 512 - 64,
                y = distance * sin(angles.fire) + 512 - 64,
                img = gfx.planetsOverworld.firePlanet,
                glow = gfx.planetsOverworld.fireGlow
            },
            air = {
                x = distance * cos(angles.air) + 512 - 64,
                y = distance * sin(angles.air) + 512 - 64,
                img = gfx.planetsOverworld.airPlanet,
                glow = gfx.planetsOverworld.airGlow
            },
            water = {
                x = distance * cos(angles.water) + 512 - 64,
                y = distance * sin(angles.water) + 512 - 64,
                img = gfx.planetsOverworld.waterPlanet2,
                glow = gfx.planetsOverworld.waterGlow
            },
            grass = {
                x = distance * cos(angles.grass) + 512 - 64,
                y = distance * sin(angles.grass) + 512 - 64,
                img = gfx.planetsOverworld.grassPlanet,
                glow = gfx.planetsOverworld.grassGlow
            },
            hell = {
                x = 512 - 64,
                y = 512 - 64,
                img = gfx.planetsOverworld.hellPlanet,
                glow = gfx.planetsOverworld.pentagon
            }
        }
    end
end


-- On map update
function specific.update(cMap, dt)
    if cMap == "overworld" then
        health = healthMax
        gotKey = false
        local planetNames = { "dust", "fire", "air", "water", "grass" }
        for i = 1, 5 do
            planet = planets[planetNames[i]]
            if planet.x < player.x and player.x < planet.x + 128 and
               planet.y < player.y and player.y < planet.y + 128 and
               not world.opened[planetNames[i]] then
                canTalk = true
                if input.check("action") then
                    sfx.planetEnter:play()
                    world.current = planetNames[i]
                    fadeStart(maps[planetNames[i]]["hub"])
                    --map.change((maps[planetNames[i]]["hub"]))
                end
            end
        end

        if countOpenWorlds() == 5 then
            if planets.hell.x < player.x and player.x < planets.hell.x + 128 and
               planets.hell.y < player.y and player.y < planets.hell.y + 128 then
                a.stop()
                sfx.planetEnter:play()
                world.current = "hell"
                fadeStart(maps["hell"].hub)
            end
        end

        if specific.width == nil then
            specific.width = 2
        end
        if specific.width < math.random(3,6) then
                specific.fluct = true
        end
        if specific.width > math.random(7,9) then
                specific.fluct = false
        end
        if specific.fluct then
            specific.width = specific.width + math.random(2, 8) * dt
        end
        if not specific.fluct then
            specific.width = specific.width - math.random(2, 8) * dt
        end
    end
end


-- On map draw
function specific.draw(cMap, dt)
    if cMap == "overworld" then

        local d = love.graphics.draw
        connDust, connFire, connAir, connWater, connGrass = false
        local dust = planets.dust
        local fire = planets.fire
        local air = planets.air
        local water = planets.water
        local grass = planets.grass


        if world.opened.dust then
            d(dust.glow, dust.x - 32, dust.y - 32)
            connDust = true
        end
        if world.opened.fire then
            d(fire.glow, fire.x - 32, fire.y - 32)
            connFire = true
        end
        if world.opened.air then
            d(air.glow, air.x - 32, air.y - 32)
            connAir = true
        end
        if world.opened.water then
            d(water.glow, water.x - 32, water.y - 32)
            connWater = true
        end
        if world.opened.grass then
            d(grass.glow, grass.x - 32, grass.y - 32)
            connGrass = true
        end

        if countOpenWorlds() == 5 then
            love.graphics.draw(planets.hell.glow, planets.hell.x - 83, planets.hell.y - 83, 0, 1.05)
            love.graphics.draw(planets.hell.img, planets.hell.x, planets.hell.y)
        end

        love.graphics.setLineWidth(specific.width or 2)
        d = love.graphics.line
        if connDust then
            if connFire then
                d(dust.x + 64, dust.y + 64, fire.x + 64, fire.y + 64)
            end
            if connAir then
                d(dust.x + 64, dust.y + 64, air.x + 64, air.y + 64)
            end
            if connWater then
                d(dust.x + 64, dust.y + 64, water.x + 64, water.y + 64)
            end
            if connGrass then
                d(dust.x + 64, dust.y + 64, grass.x + 64, grass.y + 64)
            end
        end
        if connFire then
            if connAir then
                d(fire.x + 64, fire.y + 64, air.x + 64, air.y + 64)
            end
            if connWater then
                d(fire.x + 64, fire.y + 64, water.x + 64, water.y + 64)
            end
            if connGrass then
                d(fire.x + 64, fire.y + 64, grass.x + 64, grass.y + 64)
            end
        end
        if connAir then
            if connWater then
                d(air.x + 64, air.y + 64, water.x + 64, water.y + 64)
            end
            if connGrass then
                d(air.x + 64, air.y + 64, grass.x + 64, grass.y + 64)
            end
        end
        if connWater and connGrass then
                d(water.x + 64, water.y + 64, grass.x + 64, grass.y + 64)
        end

        love.graphics.setLineWidth(1)

        love.graphics.draw(planets.dust.img, planets.dust.x, planets.dust.y)
        love.graphics.draw(planets.fire.img, planets.fire.x, planets.fire.y)
        love.graphics.draw(planets.air.img, planets.air.x, planets.air.y)
        love.graphics.draw(planets.water.img, planets.water.x, planets.water.y)
        love.graphics.draw(planets.grass.img, planets.grass.x, planets.grass.y)

    end
end

-- On map kill
function specific.kill(cMap)

end
