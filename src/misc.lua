-- chikun :: 2014
-- Miscellaneous functions


-- Convert resource from string to actual data
-- eg. 'gfx.shrek' to gfx.shrek's image data
function getResourceFromString(resource)

    -- Remove unneccessary parts of filename
    resource = resource:gsub(".jpg", "")
    resource = resource:gsub(".ogg", "")
    resource = resource:gsub(".png", "")
    resource = resource:gsub("%.%./", "")

    -- Replace slashes with periods to simplify loading
    resource = resource:gsub("/", "%.")

    -- Convert current string to table of strings
    tab = { }
    for word in resource:gmatch("([^.]+)") do
        tab[#tab + 1] = word
    end

    -- Recursively access deeper parts of table containing resource
    for key, value in ipairs(tab) do
        if key == 1 then
            resource = _G[value]
        else
            resource = resource[value]
        end
    end

    -- Return the found resource
    return resource

end

function getDistance(obj1, obj2)
    dx = obj1.x + 24 - obj2.x + 64
    dy = obj1.y + 24 - obj2.y + 64
    dist = math.sqrt(dx * dx + dy * dy)
    return dist
end
