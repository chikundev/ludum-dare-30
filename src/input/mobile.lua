-- chikun :: 2014
-- Handles input for Android devices

-- The table that houses input functions
input = { }

-- Table to hold OnScreen Control layout and functions
osc = { }

osc.toDraw = false

osc.image = {
    dpad = love.graphics.newImage("gfx/androidDPad.png"),
    btnA = love.graphics.newImage("gfx/androidBtnA.png"),
    btnB = love.graphics.newImage("gfx/androidBtnB.png"),
    menu = love.graphics.newImage("gfx/androidMenu.png")
}

function osc.load()


    --local _, _, flags = love.window.getMode()
    --local screenX, screenY = love.window.getDesktopDimensions(flags.display)

    local screenX = love.window.getWidth()
    local screenY = love.window.getHeight()


    osc.padSize = screenX / 4
    osc.btnSize = screenX / 10
    osc.menuSize = 9 * screenX / 80

    osc.padX = 0
    osc.padY = screenY - osc.padSize
    osc.btnAX = screenX - 1.5 * osc.btnSize
    osc.btnAY = screenY - 1.5 * osc.btnSize
    osc.btnBX = screenX - 3 * osc.btnSize
    osc.btnBY = screenY - 1.5 * osc.btnSize
    osc.menuX = screenX / 2 - osc.menuSize / 2
    osc.menuY = 9 / 10 * screenY

    osc.scale = screenX / 1920

    osc.toDraw = true

end


function osc.draw()
    if osc.toDraw == true then
        g.setColor(255, 255, 255)
        love.graphics.draw( osc.image.dpad, osc.padX, osc.padY, 0, osc.scale)
        love.graphics.draw( osc.image.btnA, osc.btnAX, osc.btnAY, 0, osc.scale)
        love.graphics.draw( osc.image.btnB, osc.btnBX, osc.btnBY, 0, osc.scale)
        love.graphics.draw( osc.image.menu, osc.menuX, osc.menuY, 0, osc.scale)
    end
end

-- Table of key tables
keys = {
--  Action = { left x, right x, top y, bottom y },
    down =   { 0.05, 0.20, 0.82, 1.00 },
    left =   { 0.00, 0.10, 0.64, 0.91 },
    right =  { 0.15, 0.25, 0.64, 0.91 },
    up =     { 0.05, 0.20, 0.55, 0.73 },
    action = { 0.85, 0.95, 0.76, 0.92 },
    back =   { 0.70, 0.80, 0.76, 0.92 },
    inv =    { 0.45, 0.55, 0.90, 1.00 },
    menu =   { 0,0,0,0 },
    leftBump = { 0,0,0,0 },
    rightBump = { 0,0,0,0 }
}



-- Actually checks if a button is pressed
function input.check(inputType)

    -- Our return value
    local keyPressed = false

    if love.touch.getTouchCount() > 0 and inputType ~= nil then
        for index = 1, love.touch.getTouchCount() do
            local id, x, y, pressure = love.touch.getTouch(index)
            if  keys[inputType][1] < x and
                x < keys[inputType][2] and
                keys[inputType][3] < y and
                y < keys[inputType][4] then
                    keyPressed = true
            end
        end
    end

    return keyPressed

end
