-- chikun :: 2014
-- Load and update enemies


-- Holds all enemy functions
healthDrop = { }
enemy = { }

ene = { }
ene.projectile = { }
local bossTimer = 0
local bossTimerMax = 3
vul = 0
vulDir = 0
-- Holds all enemies
enemies = { }


-- Update all enemies
function enemy:update(dt)

    vul = vul + (vulDir * dt)
    if vul > 2 then
        vul = 0
    end

    -- Iterate through enemies and run all update functions
    for key, minion in ipairs(enemies) do
        if minion.name == "broodmother" and minion.type == "dust" then
            bossTimer = 0
        end
        if minion.hurt then
            bossTimer = 0
            bossTimerMax = 3
            minion.rootTimer = 0
            minion.stuck = false
            minion.hurt = false
        end
        local angle = 0
        local run = player.damage + minion.shove
        -- Moving enemies towards the player
        if math.dist(minion.x, minion.y, player.x, player.y) <= 256 or minion.boss then

            -- Calculate angle between enemy and player
            angle = math.atan2(minion.y - player.y, minion.x - player.x)
            enemy.gfxWork(dt, angle, minion)
            local chance = math.random(100)
            if minion.shot > 0 then
                minion.shot = minion.shot - dt
                if minion.shot <= 0 then
                    minion.shot = 0
                end
            end

            if (minion.name == "pirate" or minion.name == "fireGhost" or minion.name == "plant" or minion.name == "fan" or minion.boss) and minion.shot == 0 then
                minion.shot = 1

                if  chance >= 50 and not minion.boss then

                    ene.projectile[#ene.projectile + 1] = {
                        x = minion.x,
                        y = minion.y,
                        w = 16,
                        h = 16,
                        xMove = math.cos(angle),
                        yMove = math.sin(angle),
                        image = power.image[world.current],
                        dmg = countOpenWorlds()
                    }
                end

                if minion.boss and not minion.stuck then
                    minion.shot = 1
                    bossTimer = bossTimer + 1
                    if bossTimer == bossTimerMax then
                        minion.stuck = true
                        minion.rootTimer = 10
                        bossTimerMax = bossTimerMax + 1
                        bossTimer = 0
                    end
                    for i = 1, countOpenWorlds() * 2 do
                        ene.projectile[#ene.projectile + 1] = {
                            x = minion.x + ((minion.w / (countOpenWorlds() * 2 - 1)) * (i - 1)),
                            y = minion.y,
                            w = 16,
                            h = 16,
                            xMove = 0,
                            yMove = 1,
                            image = power.image[minion.type],
                            dmg = countOpenWorlds()
                        }
                        ene.projectile[#ene.projectile + 1] = {
                            x = minion.x + ((minion.w / (countOpenWorlds() * 2 - 1)) * (i - 1)),
                            y = minion.y + minion.h,
                            w = 16,
                            h = 16,
                            xMove = 0,
                            yMove = -1,
                            image = power.image[minion.type],
                            dmg = countOpenWorlds()
                        }
                        ene.projectile[#ene.projectile + 1] = {
                            x = minion.x,
                            y = minion.y + ((minion.h / (countOpenWorlds() * 2 - 1)) * (i - 1)),
                            w = 16,
                            h = 16,
                            xMove = 1,
                            yMove = 0,
                            image = power.image[minion.type],
                            dmg = countOpenWorlds()
                        }
                        ene.projectile[#ene.projectile + 1] = {
                            x = minion.x + minion.w,
                            y = minion.y + ((minion.h / (countOpenWorlds() * 2 - 1)) * (i - 1)),
                            w = 16,
                            h = 16,
                            xMove = -1,
                            yMove = 0,
                            image = power.image[minion.type],
                            dmg = countOpenWorlds()
                        }
                    end
                    for i = 1, countOpenWorlds() * 2 - 1 do
                        ene.projectile[#ene.projectile + 1] = {
                            x = minion.x,
                            y = minion.y,
                            w = 16,
                            h = 16,
                            xMove = 0.5 + (0.5 * (i / (countOpenWorlds() * 2 - 1))),
                            yMove = 0.5 + (0.5 * ((countOpenWorlds() * 2 - 1 - i)/ (countOpenWorlds() * 2 - 1))),
                            image = power.image[minion.type],
                            dmg = countOpenWorlds()
                        }
                        ene.projectile[#ene.projectile + 1] = {
                            x = minion.x,
                            y = minion.y + minion.h,
                            w = 16,
                            h = 16,
                            xMove = 0.5 + (0.5 * (i / (countOpenWorlds() * 2 - 1))),
                            yMove = - 0.5 - (0.5 * ((countOpenWorlds() * 2 - 1 - i)/ (countOpenWorlds() * 2 - 1))),
                            image = power.image[minion.type],
                            dmg = countOpenWorlds()
                        }
                        ene.projectile[#ene.projectile + 1] = {
                            x = minion.x + minion.w,
                            y = minion.y,
                            w = 16,
                            h = 16,
                            xMove = - 0.5 - (0.5 * (i / (countOpenWorlds() * 2 - 1))),
                            yMove = 0.5 + (0.5 * ((countOpenWorlds() * 2 - 1 - i)/ (countOpenWorlds() * 2 - 1))),
                            image = power.image[minion.type],
                            dmg = countOpenWorlds()
                        }
                        ene.projectile[#ene.projectile + 1] = {
                            x = minion.x + minion.w,
                            y = minion.y + minion.h,
                            w = 16,
                            h = 16,
                            xMove = - 0.5 - (0.5 * (i / (countOpenWorlds() * 2 - 1))),
                            yMove = - 0.5 - (0.5 * ((countOpenWorlds() * 2 - 1 - i)/ (countOpenWorlds() * 2 - 1))),
                            image = power.image[minion.type],
                            dmg = countOpenWorlds()
                        }
                    end
                end
            end


            if not minion.stuck then

                -- Move on xaxis based on angle
                if minion.name ~= "plant" and minion.name ~= "fan" then
                    minion.x = math.round(minion.x - (64 * math.cos(angle) * dt * ( 1 - run)))


                    -- Prevent collision with walls
                    if math.overlapTable(collisions, minion) then
                        minion.x = math.round(minion.x + (64 * math.cos(angle) * dt * (1 - run)))
                    end

                    -- Prevent collisions with other enemies
                    for keys, minions in ipairs(enemies) do
                        if key ~= keys then
                            if math.overlap(minion, minions) then
                                minion.x = math.round(minion.x + (64 * math.cos(angle) * dt * (1 - run)))
                            end
                        end
                    end

                    -- Move on yaxise based on angle
                    minion.y = math.round(minion.y - (64 * math.sin(angle) * dt * (1 - run)))

                    -- Prevent collisions with walls
                    if math.overlapTable(collisions, minion) then
                        minion.y = math.round(minion.y + (64 * math.sin(angle) * dt * (1 - run)))
                    end

                    -- Prevent collisions with other enemies
                    for keys, minions in ipairs(enemies) do
                        if key ~= keys then
                            if math.overlap(minion, minions) then
                                minion.y = math.round(minion.y + (64 * math.sin(angle) * dt * (1 - run)))
                            end
                        end
                    end
                end
            end
        else
            -- Set graphical stage to default
            minion.gfxStep  = 1
            minion.facing   = 1
        end

        -- Damage player and run if collision
        if math.overlap(player, minion) and not (input.check("back") and not defenceDelay and power.defence) and player.damage == 0 then
            player.damage = 3
            health = health - countOpenWorlds()
        end

        -- Dealing with being shoved
        if minion.shove > 0 then
            if minion.boss then
                minion.shove = 0
            end
            minion.shove = minion.shove - dt
            if minion.shove <= 0 then
                minion.shove = 0
            end
        end

        -- Rooting deals damage
        if minion.stuck then
            if not minion.boss then
                minion.hp = minion.hp - (1 * dt / countOpenWorlds())
            end
            minion.rootTimer = minion.rootTimer - dt
            if minion.rootTimer <= 0 then
                minion.stuck = false
                minion.rootTimer = 0
            end
        end

        -- Making enemies die if no health
        if minion.hp <= 0 then
            local chance = math.random(100)
            if minion.name == "knights" then
                chat.start(txt[world.current .. "BossDefeat"])
            elseif chance <= 5 then
                healthDrop[#healthDrop + 1] = {
                    x = minion.x + minion.w / 2 - 8,
                    y = minion.y + minion.h / 2 - 8,
                    w = 16,
                    h = 16,
                    image = gfx.healthDrop
                }
            end
            table.remove(enemies, key)
        end

        --Update.enemies()
    end

    for key, projectile in ipairs(ene.projectile) do
        projectile.x = projectile.x - (projectile.xMove * 128 * dt)
        projectile.y = projectile.y - (projectile.yMove * 128 * dt)
        if math.overlap(player, projectile) and not (input.check("back") and not defenceDelay) then
            table.remove(ene.projectile, key)
            if player.damage == 0 then
                player.damage = 3
                health = health - countOpenWorlds()
            end
        end
        if math.overlapTable(collisions, projectile) then
            table.remove(ene.projectile, key)
        end
    end

end


function enemy:load(spawnSpotx, spawnSpoty, worldCall)
--loading enemy
    local element = world.current
    print(element .. "3")
    if element == "hell" then
        print(element .. "1")
        element = "water"
    end
    print(element .. "2")
    enemies[#enemies+1] = {
        name        = worldCall.name,
        x           = spawnSpotx,
        y           = spawnSpoty,
        w           = worldCall.w,
        h           = worldCall.h,
        gfxTable    = worldCall.gfxTable,
        gfxStep     = 1,
        stuck       = false,
        shove       = 0,
        rootTimer   = 0,
        -- Adding variable hp
        hp          = worldCall.hp,
        shot        = 0,
        facing      = 1,
        boss        = false,
        type        = element
    }

end

function enemy:draw()

    enemyDrawGFX()

    for key, drop in ipairs(healthDrop) do
        g.setColor(255, 255, 255)
        g.draw(drop.image, drop.x, drop.y)
    end

end

-- Kill all enemies
function enemy:clear()

    -- Let's do this shit
    enemies = { }

end

function enemy:spawn()

    -- Spawn boss mob specifically
    for key, spawnArea in ipairs(enemySpawns) do
        if spawnArea.name == "bossSpawn" then
            local boss = characters.enemies[world.current][#characters.enemies[world.current]]
            enemy:load(spawnArea.x, spawnArea.y, boss)
            enemies[#enemies].boss = true
        else
            --Called to grab the tile width count and tile height count and then total tile count
            --of overall area on tile map.
            local enemyTileWidth = spawnArea.w / map.current.tileW
            local enemyTileHeight = spawnArea.h / map.current.tileH
            local enemyTileCount = enemyTileWidth * enemyTileHeight

            --enemy min and max according to tile count
            local enemyMax = (enemyTileCount / 18) --* difficulty
            local enemyMin = (enemyTileCount / 36) --* diificulty

            --determines enemy count randomly
            local enemyCount = math.random(enemyMin, enemyMax)

            --determines random spawn spots for enemies.
            for i = 1, enemyCount do

                --Works out enemy type
                local randomEnemy = math.random(1, #characters.enemies[world.current] - 1)
                local worldCall = characters.enemies[world.current][randomEnemy]

                --Works out enemy location
                local spawnSpotx = math.random(spawnArea.x, (spawnArea.x+spawnArea.w))
                local spawnSpoty = math.random(spawnArea.y, (spawnArea.y+spawnArea.h))
                local enemyW = worldCall.w
                local enemyH = worldCall.h
                local enemyObj = {
                    x = spawnSpotx,
                    y = spawnSpoty,
                    w = enemyW,
                    h = enemyH
                }

                --While loop
                while math.overlapTable(enemies, enemyObj) do
                --Works out enemy location
                    spawnSpotx = math.random(spawnArea.x, (spawnArea.x+spawnArea.w))
                    spawnSpoty = math.random(spawnArea.y, (spawnArea.y+spawnArea.h))
                    enemyW = worldCall.w
                    enemyH = worldCall.h
                    enemyObj = {
                        x = spawnSpotx,
                        y = spawnSpoty,
                        w = enemyW,
                        h = enemyH
                    }
                end

                -- Uses maths, working out and randomization to load and draw enemies.
                enemy:load(spawnSpotx, spawnSpoty, worldCall)
            end
        end
    end
end


function enemy.gfxWork(dt, angle, minion)

    gfxWork(dt, angle, minion)

end
