-- chikun :: 2014
-- Core routines


core = { }      -- Create core table


function core.load()

    canTalk = false

    -- Control world events
    world = {
        current = "water",
        currentMap = "",
        opened = {
            water   = false,
            fire    = false,
            dust    = false,
            air     = false,
            grass   = false
        }
    }

    gotKey = false

    -- Load all maps
    require "src/load/maps"

    -- Reset view
    resetView()

    -- Global variables
    gravity = 1800
    volume = 0.5

    -- Set our level number
    level = 1

    -- Load play state
    state.load(states.splash)

    -- Implementing health
    health = 20
    healthMax = 20

    grassLoad = true
    fireLoad = true
    dustLoad = true
    airLoad = true
    hellLoad = true

end


function core.update(dt)

    -- Set can talk to false
    canTalk = false
    -- Update volume to current values
    a.setVolume(volume)

end


-- Draws underlay
function core.underlay()

end


-- Draws overlay
function core.overlay()

    -- Perform scaling function
    scale.perform()

    -- Draw overlay
    if state.current == states.play then
        power.overlay()

        -- Show current health versus max health
        g.setColor(128, 0, 0)
        g.rectangle("fill", 45, 15, healthMax * 5, 20)
        g.setColor(0, 128, 0)
        g.rectangle("fill", 45, 15, health * 5, 20)

        if gotKey then
            g.setColor(255, 255, 255)
            g.draw(gfx.templeKey, 640 - 32, 16, 0, 0.5, 0.5)
        end

    end

    -- Reset graphics
    g.origin()
    g.setScissor()

    if s.getOS() == "Android" then
        osc.draw()
    end


end


function resetView()

    -- Create view object
    view = {
        x = 0,
        y = 0,
        w = 640,
        h = 360
    }

end


-- Return number of open worlds
function countOpenWorlds()

    local worldsOpened = 1

    if world.opened.fire then
        worldsOpened = worldsOpened + 1
    end
    if world.opened.dust then
        worldsOpened = worldsOpened + 1
    end
    if world.opened.air then
        worldsOpened = worldsOpened + 1
    end
    if world.opened.grass then
        worldsOpened = worldsOpened + 1
    end

    return worldsOpened

end
