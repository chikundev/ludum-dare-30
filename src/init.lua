-- chikun :: 2014
-- Load required libraries


-- Load shorthand bindings
require "src/bindings"

-- Load maths functions
require "src/maths"

-- Load miscellaneous functions
require "src/misc"

-- Load core functions
require "src/core"

-- Load resources
require "src/load/dialogue" -- Load dialogue    recursive
require "src/load/fonts"    -- Load fonts       NOT recursive
require "src/load/gfx"      -- Load graphics    recursive
require "src/load/snd"      -- Load sounds      recursive
require "src/load/states"   -- Load states      recursive

-- Load scaling functions
require "src/scaling"

-- Load state manager
require "src/stateManager"

-- Load input manager, depending on OS
if s.getOS() == "Android" then
    require "src/input/mobile"
else
    require "src/input/desktop"
end

-- Load player functions
require "src/player"

-- Load enemy functions
require "src/enemy"

-- Load power functions
require "src/power"

-- Load characters
require "src/characters/init"

-- Load specifics
require "src/specific"

-- Load specifics
require "src/gfxWork"
