-- chikun :: 2014
-- Credits state


-- Temporary state, removed at end of script
local newState = { }


-- On state create
function newState:create()

    scroll = 0

    credits = [[
    STAR TURTLE 64
    ==============

    * Writers *
    Josef Frank / Ryan Liddle / Mark Moore

    * Programmers *
    Chris Alderton / Josef Frank
    Bradley Roope / Gage Wilkins

    * Map Design *
    Mathew Dwyer

    * Graphical Design *
    Mathew Dwyer / Mark Moore / Bradley Roope

    * Audio *
    Cohen Dennis / Josef Frank / Ryan Liddle

    * Chikun is: *
    Chris Alderton / Josef Frank
    Mathew Dwyer / Ryan Liddle
    Bradley Roope / Cohen Dennis
    Mark Moore / Gage Wilkins]]

    bgm.mainMenu:stop()
    bgm.credits:setLooping(false)
    bgm.credits:play()

end


-- On state update
function newState:update(dt)

    scroll = scroll + dt * 20
    if scroll >= 1250 then
        state.change(states.menu)
    end

end


-- On state draw
function newState:draw()

    scale.perform()

    g.setFont(fnt.menu)
    g.setColor(255, 255, 255)
    g.printf(credits, 0, 360 - scroll, 640, 'center')
    local alpha = 255
    if scroll > 1200 then
        alpha = (1250 - scroll) * 2.55 * 2
    end
    g.setColor(255, 255, 255, alpha)
    g.printf("Thank You", 0, math.max(360 - scroll+900, 180), 640, 'center')

end


-- On state kill
function newState:kill()

    scroll = nil
    bgm.credits:stop()

end


-- Transfer data to state loading script
return newState
