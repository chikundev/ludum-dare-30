-- chikun :: 2014
-- Inventory state


-- Temporary state, removed at end of script
local inventoryState = { }
local scalemodes = {"whole", "full", "ratio"}
local scalenumber = 0

selected = 6
selectedTimer = 0
inventory = { }
local selec = 1
local selecte = 2

-- On state create
function inventoryState:create()

    open = false
    sel = selected

    inventory = {
        x =  50,
        y =  100 - view.h,
        w = view.w - 100,
        h = view.h - 100
    }

end

local timer = 0
local tab = 1
-- On state update
function inventoryState:update(dt)

    if timer > 0 then
        timer = timer - dt
        if open then
            inventory.y = inventory.y - 602 * dt
        else
            inventory.y = inventory.y + 602 * dt
        end
        if timer <= 0 then
            timer = 0
            if open then
                inventoryState:kill()
                state.current = states.play
            else
                open = true
            end

        end

    end

    if selectedTimer > 0 then
        selectedTimer = selectedTimer - dt
        if selectedTimer <= 0 then
            selectedTimer = 0
        end
    end

    -- Select diffent powers
    if not areYouSure and not optionOpen then
        if tab == 1 then
            if (input.check("up") or input.check("down")) and selectedTimer == 0 then
                sfx.menuMove:stop()
                sfx.menuMove:play()
                local dir = 0
                if input.check("down") then
                    dir = 1
                else
                    dir = -1
                end
                sel = sel + dir
                if sel == 5 then
                    sel = sel + dir
                end
                if sel >= 7 then
                    sel = 1
                elseif sel <= 0 then
                    sel = 6
                end
                while not power.check(sel) do
                    sel = sel + dir
                    if sel >= 7 then
                        sel = 1
                    elseif sel <= 0 then
                        sel = 6
                    end
                end
                selectedTimer = 0.2
                power.current = selected
            end
        end

        -- Cycle through the menu in inventory
        if tab == 2 then
            if (input.check("up") or input.check("down")) and selectedTimer == 0 then
                sfx.menuMove:stop()
                sfx.menuMove:play()
                local dir = 0
                if input.check("down") then
                    dir = 1
                else
                    dir = -1
                end
                selec = selec + dir
                if selec >= 4 then
                    selec = 1
                end
                if selec <= 0 then
                    selec = 3
                end
                selectedTimer = 0.2
            end
        end

        if input.check("left") and selectedTimer == 0 then
                sfx.menuMove:stop()
                sfx.menuMove:play()
            tab = tab - 1
            if tab <= 0 then
                tab = 2
            end
            selectedTimer = 0.2
        elseif input.check("right") and selectedTimer == 0 then
                sfx.menuMove:stop()
                sfx.menuMove:play()
            tab = tab + 1
            if tab >= 3 then
                tab = 1
            end
            selectedTimer = 0.2
        end

        if timer == 0 and ((input.check("back") or input.check("action") or input.check("inv")) or not open) and selectedTimer == 0 then
            if input.check("action") and open then
                sfx.menuBlip:stop()
                sfx.menuBlip:play()
                if tab == 1 then
                    selected = sel
                    timer = 0.5
                elseif tab == 2 then
                    if selec == 1 or selec == 3 then
                        areYouSure = true
                        selecte = 2
                        selectedTimer = 0.5
                    elseif selec == 2 then
                        optionOpen = true
                        selec = 1
                    end
                end
            else
                timer = 0.5
            end
        end

    elseif areYouSure and not optionOpen then
        if (input.check("left") or input.check("right")) and selectedTimer == 0 then
                sfx.menuMove:stop()
                sfx.menuMove:play()
            selectedTimer = 0.2
            selecte = selecte + 1
            if selecte >= 3 then
                selecte = 1
            end
        end
        if input.check("action") and selectedTimer == 0 then
            sfx.menuBlip:stop()
            sfx.menuBlip:play()
            if selecte == 1 then
                if selec == 1 then
                    tab = 1
                    state.change(states.menu)
                elseif selec == 3 then
                    e.quit()
                end
            elseif selecte == 2 then
                selectedTimer = 0.5
                areYouSure = false
            end
        end

    elseif optionOpen and not areYouSure then

        if scale.mode == "whole" then
            scalenumber = 1
        elseif scale.mode == "full" then
            scalenumber = 2
        elseif scale.mode == "ration" then
            scalenumber = 3
        end

        if (input.check("up") or input.check("down")) and selectedTimer == 0 then
            sfx.menuMove:stop()
            sfx.menuMove:play()
            local dir = 0
            if input.check("up") then
                dir = dir - 1
            end
            if input.check("down") then
                dir = dir + 1
            end
            selec = selec + dir
            if selec >= 4 then
                selec = 1
            end
            if selec <= 0 then
                selec = 3
            end
            selectedTimer = 0.2
        end

        if input.check("action") and selectedTimer == 0 then
            sfx.menuBlip:stop()
            sfx.menuBlip:play()
            if selec == 3 then
                selec = 2
                optionOpen = false
            end
            selectedTimer = 0.2
        end

        if (input.check("left") or input.check("right")) and selectedTimer == 0 then
                sfx.menuMove:stop()
                sfx.menuMove:play()
            local dir = 0
            if input.check("left") then
                dir = dir - 1
            end
            if input.check("right") then
                dir = dir + 1
            end
            if selec == 1 then
                scalenumber = scalenumber + dir
                if scalenumber >= 4 then
                    scalenumber = 1
                end
                if scalenumber <= 0 then
                    scalenumber = 3
                end
                scale.mode = scalemodes[scalenumber]
            elseif selec == 2 then
                volume = volume + (dir / 10)
                if volume > 1 then
                    volume = 1
                end
                if volume < 0 then
                    volume = 0
                end
            end
            selectedTimer = 0.2
        end

        if timer == 0 and (input.check("inv") or input.check("back")) and selectedTimer == 0 then
            optionOpen = false
            selec = 2
            timer = 0.5
        end
    end
end


-- On state draw
function inventoryState:draw()

    g.setColor(255, 255, 255)
    states.play:draw()
    scale.perform()
    g.setColor(0, 0, 0)
    g.setFont(fnt.chat)
    g.rectangle("fill", inventory.x, inventory.y, inventory.w, inventory.h)
    g.setColor(255, 255, 0, 128)
    g.rectangle("fill", inventory.x + 10, inventory.y + 10 + ((selected -1) * 40), 150, 30)

    if power.check(1) then
        g.setColor(255, 255, 255)
        g.draw(gfx.projectile.waterShot, inventory.x + 20, inventory.y + 20)
        g.print("Water Shot", inventory.x + 40, inventory.y + 20)
    end
    if power.check(2) then
        g.setColor(255, 255, 255)
        g.draw(gfx.projectile.fireBreath, inventory.x + 20, inventory.y + 60)
        g.print("Fire Breath", inventory.x + 40, inventory.y + 60)
    end
    if power.check(3) then
        g.setColor(255, 255, 255)
        g.draw(gfx.projectile.airShove, inventory.x + 20, inventory.y + 100)
        g.print("Slam Dunk", inventory.x + 40, inventory.y + 100)
    end
    if power.check(4) then
        g.setColor(255, 255, 255)
        g.draw(gfx.projectile.wildNature, inventory.x + 20, inventory.y + 140)
        g.print("Wild Nature", inventory.x + 40, inventory.y + 140)
    end
    if power.check(5) then
        g.setColor(255, 255, 255)
        g.draw(gfx.projectile.whirlwind, inventory.x + 16, inventory.y + 180)
        g.setColor(255, 255, 255)
        g.print("Whirl Wind", inventory.x + 40, inventory.y + 180)
    end
    g.setColor(128, 128, 128)
    g.circle("fill", inventory.x + 28, inventory.y + 228, 8, 32)
    g.setColor(255, 255, 255)
    g.print("Nothing", inventory.x + 40, inventory.y + 220)

    -- Selecting Rectangle
    local alpha = 255
    if tab ~= 1 then
        alpha = 0
    end
    g.setColor(0, 0, 255, alpha)
    g.rectangle("line", inventory.x + 10, inventory.y + 10 + ((sel - 1) * 40), 150, 30)

    -- Menu things in inventory
    g.setColor(255, 255, 255)
    g.print("Main Menu", inventory.x + inventory.w - 120, inventory.y + 50)
    g.print("Options", inventory.x + inventory.w - 120, inventory.y + 100)
    g.print("Exit Game", inventory.x + inventory.w - 120, inventory.y + 150)
    alpha = 255
    if tab ~= 2 then
        alpha = 0
    end
    g.setColor(0, 0, 255, alpha)
    g.rectangle("line", inventory.x + inventory.w - 125, inventory.y + 50 + (50 * (selec - 1)), 100, 30)

    if areYouSure then
        g.setColor(255, 255, 255)
        g.rectangle("fill", view.w/2 - 80, view.h/2 - 50, 160, 100)
        g.setColor(0, 0, 0)
        g.print("Are you sure?", view.w/2 - 70, view.h/2 - 40)
        g.print("Yes", view.w/2 - 70, view.h/2)
        g.print("No", view.w/2 + 30, view.h/2)
        g.rectangle("line", view.w/2 - 75 + (100 * (selecte - 1)), view.h/2 - 5, 35, 30)
    end

    if optionOpen then
        g.setColor(0, 0, 0)
        g.rectangle("fill", inventory.x + inventory.w - 130, inventory.y, 125, inventory.h)
        g.setColor(255, 255, 255)
        local vol = (volume * 100) .. "%"
        g.print("Volume: " .. vol, inventory.x + inventory.w - 120, inventory.y + 100)
        g.print("Scale: " .. scale.mode, inventory.x + inventory.w - 120, inventory.y + 50)
        g.print("Back", inventory.x + inventory.w - 120, inventory.y + 150)
        g.setColor(0, 0, 255)
        g.rectangle("line", inventory.x + inventory.w - 125, inventory.y + (50 * selec), 125, 30)
    end

end


-- On state kill
function inventoryState:kill()

    inventory = { }

end


-- Transfer data to state loading script
return inventoryState
