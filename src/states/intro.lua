-- chikun :: 2014
-- Intro state


-- Temporary state, removed at end of script
local introState = { }


-- On state create
function introState:create()

    bgm.intro:play()

    -- Load main state
    state.load(states.play)

    playIntro = true

    -- Change map to overworld
    map.change(maps["overworld"])

    playIntro = false

    -- Dialogue on top
    chat.start(txt.intro)

end


-- On state update
function introState:update(dt)

end


-- On state draw
function introState:draw()

end


-- On state kill
function introState:kill()

end


-- Transfer data to state loading script
return introState
