-- chikun :: 2014
-- Fade between two levels


-- Temporary state, removed at end of script
local fadeState = { }


-- On state create
function fadeState:create()

    fadeTimer = 0
    fadeSwapped = false

end


-- On state update
function fadeState:update(dt)

    fadeTimer = fadeTimer + dt

    if fadeTimer >= 1 then
        state.current = states.play
        states.fadeBetween:kill()
    elseif fadeTimer >= 0.5 and not fadeSwapped then
        fadeSwapped = true
        if gameIsOver then
            hidePlayer = true
        else
            hidePlayer = nil
        end
        map.change(futureMap)
    end

end


-- On state draw
function fadeState:draw()

    states.play:draw()

    -- Scaling function
    scale.perform()

    -- Reset colour
    g.setColor(0, 0, 0, 255 - (math.abs(fadeTimer - 0.5) * 2 * 255))

    g.rectangle("fill", 0 ,0, 640, 360)

end


-- On state kill
function fadeState:kill()

    if overworldShow then
        chat.start(txt.overworld)
        overworldShow = nil
    end
    if _G[world.current .. "Load"] ~= nil then
        _G[world.current .. "Load"] = nil
        chat.start(txt[world.current])
    end

    if futureMap.name == "gameOver" then
        chat.start(txt.gameOver)
    end

    fadeTimer = nil
    fadeSwapped = nil
    futureMap = nil

end


function fadeStart(futMap)

    state.load(states.fadeBetween)
    futureMap = futMap

end


-- Transfer data to state loading script
return fadeState
