-- chikun :: 2014
-- Chat state


-- Chat table
chat = {
    current     = nil,
    stage       = 0,
    actionPressed   = true,
    arrowPressed    = true,
    tutorialCrabY = -32
}


-- Start chat function
function chat.start(dialogue)

    -- Set state to chat
    state.load(states.chat)

    -- Set current dialogue
    chat.current = dialogue

    -- Set stage to default
    chat.stage   = 0

    -- Update to new file
    chat.progress()

end


function chat.progress()

    -- Increment chat stage
    chat.stage = chat.stage + 1

    -- If chat stage larger than number of stages, kill dialogue
    if chat.stage > #chat.current then
        states.chat:kill()
    else
        local currentStage = chat.current[chat.stage]

        if currentStage.type == "text" then
            chat.scroll = 0
        elseif currentStage.type == "question" then
            chat.scroll     = 0
            chat.selection  = true
        elseif currentStage.type == "function" then
            currentStage.create(dt)
        elseif currentStage.type == "crabDown" then
            chat.tutorialCrabY = -32
            if not endGame then
                sfx.tutecrabDown:play()
            end
        elseif currentStage.type == "crabUp" then
            chat.tutorialCrabY = 200
            if not endGame then
                sfx.tutecrabUp:play()
            end
        end
    end

end


-- Manage state functions now


-- Chat state, removed at end of script
local chatState = { }


-- On state create
function chatState:create()

end


-- On state update
function chatState:update(dt)

    -- Find current stage
    local currentStage = chat.current[chat.stage]


    -- Depending on current stage, take action
    if currentStage.type == "text" or currentStage.type == "question" then
        -- Scroll text if can
        chat.scroll = math.min(chat.scroll + dt * 128, currentStage.text:len())

    elseif currentStage.type == "shift" then
        -- Move screen
        view.x = view.x + math.sign(currentStage.toX - view.x) * currentStage.speed * dt
        view.y = view.y + math.sign(currentStage.toY - view.y) * currentStage.speed * dt

        -- If screen close to target, set to target
        if math.abs(currentStage.toX - view.x) < currentStage.speed * dt then
            view.x = currentStage.toX
        end
        if math.abs(currentStage.toY - view.y) < currentStage.speed * dt then
            view.y = currentStage.toY
        end
        if view.x == currentStage.toX and view.y == currentStage.toY then
            chat.progress()
        end

    elseif currentStage.type == "function" then
        -- Move screen
        currentStage.update(dt)

    elseif currentStage.type == "crabDown" then
        -- Move screen
        chat.tutorialCrabY = chat.tutorialCrabY + dt * 200

        if chat.tutorialCrabY > 200 then
            chat.tutorialCrabY = 200
            chat.progress()
        end

    elseif currentStage.type == "crabUp" then
        -- Move screen
        chat.tutorialCrabY = chat.tutorialCrabY - dt * 300

        if chat.tutorialCrabY < -32 then
            chat.tutorialCrabY = -32
            chat.progress()
        end
    end


    -- Act if button pressed and can act
    if (not chat.actionPressed) and input.check("action") then
        -- If type is text, progress or end scroll
        if currentStage.type == "text" then
            sfx.chatProgress:play()
            if chat.scroll == currentStage.text:len() then
                chat.progress()
            else
                chat.scroll = currentStage.text:len()
            end
        elseif currentStage.type == "question" then
            sfx.chatProgress:play()
            if chat.scroll == currentStage.text:len() then
                currentStage.onRes(chat.selection)
            else
                chat.scroll = currentStage.text:len()
            end
        end
    end

    -- Act if arrow pressed and can act
    if (not chat.arrowPressed) and (input.check("up") or input.check("down")) then
        chat.selection = not chat.selection
        print(tostring(chat.selection))
    end

    -- Update pressed
    chat.actionPressed = input.check("action")
    chat.arrowPressed  = input.check("up") or input.check("down")

end


-- On state draw
function chatState:draw()

    inhibitDraw = true

    -- Draw previous state below chat
    states.play:draw()
    inhibitDraw = nil

    -- Fix scaling
    scale.perform()

        -- Set drawing values correctly
        g.setColor(0, 0, 0)

        if endGame then
            g.rectangle("fill", 0, 0, 640, 360)
        end

    -- If dialogue option, then draw dialogue
    local currentStage = chat.current[chat.stage]
    if currentStage.type == "text" or currentStage.type == "question" then

        -- Set drawing values correctly
        g.setColor(0, 0, 0)

        -- Calculate text height
        local textHeight = 96
        if currentStage.speaker == "" then
            textHeight = 72
        end

        if s.getOS() == "Android" then
            g.rectangle("fill", 0, 0, view.w, textHeight)
        else
            g.rectangle("fill", 0, view.h - textHeight, view.w, textHeight)
        end

        -- Set drawing values correctly
        g.setColor(255, 255, 255)
        g.setFont(fnt.chat)

        -- Width for container
        local contW = view.w - 16
        if currentStage.type == "question" then
            contW = contW - 64
        end

        if s.getOS() == "Android" then
            g.printf(currentStage.speaker .. "\n" ..
                currentStage.text:sub(1, math.floor(chat.scroll)),
                8, 0, contW, 'left')

            if currentStage.type == "question" then
                g.printf("YES\nNO", contW + 16, 24, 64, 'center')
                contY = 22
                if not chat.selection then
                    contY = contY + 18
                end
                g.rectangle("line", contW + 28, contY, 40, 18)
            end

        else
            g.printf(currentStage.speaker .. "\n" ..
                currentStage.text:sub(1, math.floor(chat.scroll)),
                8, 268, contW, 'left')

            if currentStage.type == "question" then
                g.printf("YES\nNO", contW + 16, 304, 64, 'center')
                contY = 302
                if not chat.selection then
                    contY = contY + 21
                end
                g.rectangle("line", contW + 22, contY, 52, 24)
            end

        end
    end


    g.setColor(255, 255, 255)
    local rot = math.rad(chat.tutorialCrabY * 2 - 40)
    g.draw(gfx.tutecrab, 576, chat.tutorialCrabY + 16, rot, 1, 1, 16, 16)

end


-- On state kill
function chatState:kill()

    -- Change state back
    state.current = states.play

end


-- Transfer data to state loading script
return chatState
