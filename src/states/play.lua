-- chikun :: 2014
-- Play state


-- Temporary state, removed at end of script
local playState = {
    canChangeMap = true
}

-- Temporary variable, removed at end of script
local parts = { }


-- On state create
function playState:create()

    -- Change the map
    map.change(maps["overworld"])

    -- Change background colour to fuck with people
    g.setBackgroundColor(100, 50, 125)

    -- Held action
    heldAction = false

end


-- On state update
function playState:update(dt)

    -- Update specific map
    specific.update(world.currentMap, dt)

    -- Update player
    player:update(dt)

    if player.x - view.x>630 then player.x = checkpoint.x end
    if player.y - view.y>350 then player.y = checkpoint.y end

    -- Update view
    view.x = math.clamp(0, player.x + (player.w /2) - (view.w / 2),
        mapW - view.w)
    view.y = math.clamp(0, player.y + (player.h /2) - (view.h / 2),
        mapH - view.h)

    -- Update enemy
    enemy:update(dt)

    -- Iterate over swaps (layer.objects)
    local overlap, ported = false, false
    for key, swap in ipairs(swaps or { }) do
        -- Check for collisions with swaps
        if math.overlap(player, swap) then
            overlap = true
            if states.play.canChangeMap and not ported and swap.type ~= "" then
                local parts = { }

                -- Iterate over parts in the string, splitting at #
                for part in (swap.type .. "#"):gmatch("([^#]*)#") do
                    -- Append each part to a temporary table
                    table.insert(parts, part)
                end

                cSwap = swap

                if parts[1]:find("Temple1") and parts[2] == "moveDown" and not gotKey then
                    chat.start(txt.needKey)
                    player.y = swap.y + swap.h + 4
                else
                    -- Change to new map, spawning at the door
                    map.change(getResourceFromString(parts[1]), parts[2])
                    ported = true

                    -- Prevent the player from immediately going back through the door they spawn on
                    states.play.canChangeMap = false
                end
            end
        end
    end

    if overlap == false then
        states.play.canChangeMap = true
    end


    for key, layer in ipairs(map.current.layers) do

        -- Interpret important layer
        if layer.name == "important" then
            for key, object in ipairs(layer.objects) do
                -- Place player at start
                if object.name == "key" and math.overlap(player, object) then
                    gotKey = true
                    table.remove(layer.objects, key)
                end
            end
        end
    end

    for k, v in ipairs(people) do
        if math.dist(v.x + v.w / 2, v.y + v.h / 2, player.x +
                player.w / 2, player.y + player.h / 2) < 48 then
            canTalk = true
            if input.check("action") and not heldAction then
                local text = v.name
                if v.talked then
                    text = text .. "Talked"
                end
                chat.start(txt[text])
                v.talked = true
            end
        end
    end

    -- Update heldAction
    heldAction = input.check("action")
end


-- On state draw
function playState:draw()

    if inhibitDraw == nil then
        -- Update view
        view.x = math.clamp(0, player.x + (player.w /2) - (view.w / 2),
            mapW - view.w)
        view.y = math.clamp(0, player.y + (player.h /2) - (view.h / 2),
            mapH - view.h)
    end

    -- Scaling function
    scale.perform()

    -- Reset colour
    g.setColor(255, 255, 255)

    -- Move graphics based on view
    g.translate(math.round(-view.x), math.round(-view.y))

    -- Draw map
    map.draw()

    -- Draw specific map thangzz
    specific.draw(world.currentMap)

    -- Draw enemies
    enemy:draw()

    -- Draw player
    player:draw()

    -- Prevent the player from going straight back into the door they spawn on
    -- playState.canChangeMap = false

    -- Draw projectiles
    power.draw()

    -- Reset graphical translation
    g.origin()

end


-- On state kill
function playState:kill()

    -- Kill unneeded variables
    collisions  = nil
    levelEnd    = nil
    playerStart = nil
    view        = nil
    parts       = nil

    -- Kill enemies
    enemy:clear()

    -- Kill specific map stuff
    specific.kill(world.currentMap)

end


-- Transfer data to state loading script
return playState
