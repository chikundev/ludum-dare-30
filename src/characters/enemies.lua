-- chikun :: 2014
-- Enemy characters table


return {

    water = {
        {
            name = "jellyfish",
            hp = 10,
            w = 16,
            h = 16,
            gfxTable = gfx.enemy.water.jellyfish
        },

        {
            name = "sponge",
            hp = 15,
            w = 32,
            h = 16,
            gfxTable = gfx.enemy.water.sponge
        },

        {
            name = "pirate",
            hp = 20,
            w = 32,
            h = 32,
            gfxTable = gfx.enemy.water.pirate
        },

        {
            name = "knights",
            hp = 50,
            w = 48,
            h = 48,
            gfxTable = gfx.enemy.water.knights
        },
    },

    fire = {
        {
            name = "bat",
            hp = 10,
            w = 16,
            h = 16,
            gfxTable = gfx.enemy.fire.bat
        },

        {
            name = "fireGhost",
            hp = 15,
            w = 24,
            h = 24,
            gfxTable = gfx.enemy.fire.fireghost
        },

        {
            name = "skeleton2wd",
            hp = 20,
            w = 32,
            h = 32,
            gfxTable = gfx.enemy.fire.skeleton2wd
        },


        -- placeholder till we get actual boss info
        {
            name = "knights",
            hp = 50,
            w = 48,
            h = 48,
            gfxTable = gfx.enemy.fire.knights
        }
    },

    grass = {
        {
            name = "plant",
            hp = 10,
            w = 16,
            h = 16,
            gfxTable = gfx.enemy.grass.plant
        },

        {
            name = "spider",
            hp = 15,
            w = 24,
            h = 24,
            gfxTable = gfx.enemy.grass.spider
        },

        {
            name = "dino",
            hp = 20,
            w = 32,
            h = 32,
            gfxTable = gfx.enemy.grass.dino
        },

        -- Placeholder for grass boss
        {
            name = "knights",
            hp = 50,
            w = 48,
            h = 48,
            gfxTable = gfx.enemy.grass.knights
        }
    },

    air = {
        {
            name = "cyclone",
            hp = 10,
            w = 16,
            h = 16,
            gfxTable = gfx.enemy.air.cyclone
        },

        {
            name = "fan",
            hp = 15,
            w = 24,
            h = 24,
            gfxTable = gfx.enemy.air.fan
        },

        {
            name = "jet",
            hp = 20,
            w = 32,
            h = 32,
            gfxTable = gfx.enemy.air.jet
        },

        -- Placeholder for boss
        {
            name = "knights",
            hp = 50,
            w = 48,
            h = 48,
            gfxTable = gfx.enemy.air.knights
        }
    },

    dust = {
        {
            name = "tumbleWeed",
            hp = 10,
            w = 16,
            h = 16,
            gfxTable = gfx.enemy.dust.tumbleweed
        },

        {
            name = "donkey",
            hp = 15,
            w = 24,
            h = 24,
            gfxTable = gfx.enemy.dust.donkey
        },

        {
            name = "sun",
            hp = 20,
            w = 32,
            h = 32,
            gfxTable = gfx.enemy.dust.sun
        },

        -- Place holder boss
        {
            name = "knights",
            hp = 50,
            w = 48,
            h = 48,
            gfxTable = gfx.enemy.dust.knights
        }
    },

    hell = {
        {
            name = "turtleZombieRobot",
            hp = 20,
            w = 32,
            h = 32,
            gfxTable = gfx.enemy.hell.turtlezombierobots
        },

        {
            name = "broodmother",
            hp = 100,
            w = 64,
            h = 64,
            gfxTable = gfx.enemy.hell.broodmother
        }
    }

}
