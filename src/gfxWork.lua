-- chikun :: 2014
-- Handle enemy draw additions


function gfxWork(dt, angle, minion)

    minion.facing = math.sign(math.cos(angle))

    if minion.name == "jellyfish" then
        minion.gfxStep = 2
    elseif minion.name == "pirate" then
        minion.gfxStep = math.max(minion.gfxStep + dt * 6, 2)
        if minion.gfxStep >= 4 then
            minion.gfxStep = minion.gfxStep - 2
        end
    elseif minion.name == "knights" then
        minion.gfxStep = math.max((1 + (minion.gfxStep - 1) % 2) + dt * 6, 1)
        if minion.gfxStep >= 3 then
            minion.gfxStep = minion.gfxStep - 2
        end
        if math.abs(math.cos(angle)) > math.abs(math.sin(angle)) then
            minion.gfxStep = minion.gfxStep + 2
        end
        print(minion.gfxStep)
    elseif minion.name == "dino" or minion.name == "spider" or minion.name == "bat" or minion.name == "donkey" or minion.name == "broodmother" or minion.name == "turtlezombierobots" then
        minion.gfxStep = math.max(minion.gfxStep + dt * 6, 1)
        if minion.gfxStep >= 3 then
            minion.gfxStep = minion.gfxStep - 2
        end
    elseif minion.name == "skeleton2wd" then
        minion.gfxStep = 1
        if math.abs(math.cos(angle)) > math.abs(math.sin(angle)) then
            minion.gfxStep = 2
        end
    elseif minion.name == "fan" then
        minion.gfxStep = 2
    end

end



function enemyDrawGFX()

    -- Draw minions
    for key, minion in ipairs(enemies) do
        if minion.boss and minion.stuck then
            g.setColor(255, 255 - (255 * minion.rootTimer / 10), 255 - (255 * minion.rootTimer / 10))
        end
        local offset = 0
        if minion.facing == -1 then
            offset = minion.w
        end

        -- Reset colour
        if vulDir > 0 then
            if vul > 1 then
                g.setColor(255, 0, 0)
            else
                g.setColor(255, 255, 255)
            end
        end

        g.draw(minion.gfxTable[tostring(math.floor(minion.gfxStep))], minion.x + offset, minion.y, 0, minion.facing, 1)
        g.setColor(0, 0, 0)
        g.setFont(fnt.hp)
        local offset = (100 - minion.w) / 2
        local hp = math.ceil(minion.hp * countOpenWorlds())
        if minion.name == "broodmother" then
            g.printf(minion.type, minion.x - offset + 1, minion.y - 15, 100, 'center')
            g.setColor(255, 255, 255)
            g.printf(minion.type, minion.x - offset, minion.y - 16, 100, 'center')
        else
            g.printf("HP: " .. hp, minion.x - offset + 1, minion.y - 15, 100, 'center')
            g.setColor(255, 255, 255)
            g.printf("HP: " .. hp, minion.x - offset, minion.y - 16, 100, 'center')
        end
    end

    for key, projectile in ipairs(ene.projectile) do
        -- Reset colour
        g.setColor(255, 255, 255)
        local angle = math.atan2(- projectile.yMove, - projectile.xMove)
        g.draw(gfx.projectile[projectile.image], projectile.x + 8, projectile.y + 8, angle, 1, 1, 8, 8)
    end

end
