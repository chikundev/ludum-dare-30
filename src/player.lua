-- chikun :: 2014
-- Player object


-- Create player table
player = { }
local isMoving = false

-- For player creation
function player:create(px, py, pw, ph)

    -- Implement player variables
    player.x = px
    player.y = py
    player.w = 24
    player.h = 24
    player.damage = 0
    player.speed    = 128

    -- Power variable in player
    player.projectile       = { }
    player.waterShot = { }
    player.fireBreath = { }
    player.airShove = { }
    player.wildNature = { }
    player.defence = { }
    player.defence.delay = 0

    -- Variables to shoot in 8 directions
    player.xDir = 1
    player.yDir = 0

    player.gfxTable = {
        gfx.turtle1still,
        gfx.turtle1,
        gfx.turtle1flip,
        gfx.turtle1
    }
    player.currentGFX = 1

end


-- On player update
function player:update(dt)

    if world.current == "overworld" then
        if input.check("up") or input.check("down") or input.check("left") or input.check("right") then
            isMoving = true
        else
            isMoving = false
        end
    end

    -- Calculate horizontal movement
    local xMove = 0
    if not (input.check("back") and not defenceDelay and power.defence and map.current.name ~= "overworld") then
        if input.check("left") then
            xMove = xMove - 1
        end
        if input.check("right") then
            xMove = xMove + 1
        end
    end


    -- Calculate vertical movement
    local yMove = 0
    if not (input.check("back") and not defenceDelay and power.defence and map.current.name ~= "overworld") then
        if input.check("up") then
            yMove = yMove - 1
        end
        if input.check("down") then
            yMove = yMove + 1
        end
    end

    -- Move player
    if (xMove ~= 0 or yMove ~= 0) and not (input.check("back") and not defenceDelay and map.current.name ~= "overworld") then
        player.xDir = xMove
        player.yDir = yMove
        player.currentGFX = player.currentGFX + dt * 12
        if player.currentGFX >= 5 then
            player.currentGFX = player.currentGFX - 4
        end
    else
        player.currentGFX = 1
    end

    if input.check("back") and not defenceDelay and power.defence and map.current.name ~= "overworld" then

        if shotPlayed ~= true then
            shotPlayed = true
            sfx.shotDust:play()
        end
        player.x = player.x + player.xDir * player.speed * dt * 1.5

    else
        shotPlayed = false
        sfx.shotDust:stop()
        player.x = player.x + xMove * player.speed * dt
    end

    -- Prevent player from moving inside of blocks
    while math.overlapTable(collisions, player) do
        player.x = math.round(player.x) - player.xDir
    end

    if input.check("back") and not defenceDelay and power.defence and map.current.name ~= "overworld" then

        player.y = player.y + player.yDir * player.speed * dt * 1.5

    else
        player.y = player.y + yMove * player.speed * dt
    end

    -- Prevent player from moving inside of blocks
    while math.overlapTable(collisions, player) do
        player.y = math.round(player.y) - player.yDir
    end

    -- Attempting to implement power attacks
    if not input.check("back") or defenceDelay or not power.defence and map.current.name ~= "overworld" then
        if input.check("action") and selected == 1 then
            power.create(dt, "waterShot")
        end
        if input.check("action") and selected == 2 then
            power.create(dt, "fireBreath")
        end
        if input.check("action") and selected == 3 then
            power.create(dt, "airShove")
        end
        if input.check("action") and selected == 4 then
            power.create(dt, "wildNature")
        end
    end


    power.update(dt)
    defence.update(dt)

    -- Openning inventory
    if input.check("inv") then
        state.load(states.inventory)
    end

    -- Count down till enemies attack again
    if player.damage > 0 then
        player.damage = player.damage - dt
        if player.damage <= 0 then
            player.damage = 0
        end
    end

    if math.overlapTable(healthDrop, player) then
        for key, drop in ipairs(healthDrop) do
            if math.overlap(drop, player) then
                sfx.health:play()
                health = health + 1
                if health >= healthMax then
                    health = healthMax
                end
                table.remove(healthDrop, key)
            end
        end
    end

    if health <= 0 then
        sfx.death:play()
        health = 0
        vul = 0
        vulDir = 0
        gameIsOver = true
        fadeStart(maps["gameOver"])
        local musc = getSongFromName(world.currentMap)
        if musc ~= "" then
            bgm[musc]:stop()
        end
        health = healthMax
    end

end


-- On player draw
function player.draw()

    if hidePlayer == nil then

    local alpha = 255 - (85 * player.damage)

    -- Draw basic player
    --g.setColor(200, 150, 100)
    --g.rectangle("fill", player.x, player.y, player.w, player.h)

    local defCheck = (input.check("back") and not defenceDelay and power.defence and map.current.name ~= "overworld")

    local dir = math.deg(math.atan2(player.xDir, -player.yDir))
    if defCheck then
        dir = dir + defenceTimer * 720
    end
    g.setColor(255, 255, 255, alpha)
    if map.current.name == "overworld" then
        if not isMoving then
            g.draw(gfx.ship, player.x + 16, player.y + 16, math.rad(dir), 1, 1, 16, 16)
        else
            g.draw(gfx.shipMove, player.x + 16, player.y + 16, math.rad(dir), 1, 1, 16, 16)
        end
    else
            g.draw(player.gfxTable[math.floor(player.currentGFX)], player.x + 12, player.y + 12, math.rad(dir), 1, 1, 12, 12)
    end
    if defCheck then
        defence.draw()
    end

    if canTalk then
        g.setColor(255, 255, 255)
        g.draw(gfx.actionPrompt, player.x + player.w / 2 - 4, player.y - 5)
    end
    end
end
