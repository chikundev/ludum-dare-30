-- chikun :: 2014
-- Loads all dialogue from the /txt folder


-- Recursively checks a folder for text
-- and adds any found to a table
function checkFolder(dir, tab)

    local tab = ( tab or { } )

    -- Get a table of files / subdirs from dir
    local items = f.getDirectoryItems(dir)

    for key, val in ipairs(items) do

        if f.isFile(dir .. "/" .. val) then
            -- If item is a file, then load it into tab

            -- Remove ".ogg" extension on file
            name = val:gsub(".lua", "")

            -- Load sound into table
            tab[name] = require(dir .. "/" .. name)

        else
            -- Else, run checkFolder on subdir

            -- Add new table onto tab
            tab[val] = { }

            -- Run checkFolder in this subdir
            checkFolder(dir .. "/" .. val, tab[val])

        end

    end

    return tab

end


-- Load the txt folder into the txt table
txt = checkFolder("txt")


-- Kills checkFolder
checkFolder = nil
