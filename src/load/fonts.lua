-- chikun :: 2014
-- Loads all fonts from the /fnt folder


-- Font multiplier
local multi = 1
if s.getOS() == "Android" then
    multi = 0.75
end

-- Can't do this recursively due to sizes
fnt = {
    -- Splash screen font
    splash  = g.newFont("fnt/exo2.otf", 64 * multi),

    -- Chat font
    chat    = g.newFont("fnt/monaco.ttf", 16 * multi),

    -- Menu font
    menu    = g.newFont("fnt/monaco.ttf", 24 * multi),

    -- HP font
    hp      = g.newFont("fnt/monaco.ttf", 10 * multi),
}
