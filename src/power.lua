-- chikun :: 2014
-- Making original powers test

local timer = 0

power = { }
power.waterShot = false
power.fireBreath = false
power.airShove = false
power.wildNature = false
power.defence = false
power.delay = 0
power.image = {
    water = "waterShot",
    fire = "fireBreath",
    air = "airShove",
    grass = "wildNature",
    dust = "whirlwind"
}

-- Power update
function power.update(dt)
    -- Moving projectiles
    for key, projectile in ipairs(player.projectile) do
        projectile.x = projectile.x + (256 * dt * projectile.xDir)
        projectile.y = projectile.y + (256 * dt * projectile.yDir)
    end

    -- Delay use of chained powers
    if power.delay > 0 then
        power.delay = power.delay - dt
        if power.delay <= 0 then
            power.delay = 0
        end
    end

    if selectedTimer > 0 then
        selectedTimer = selectedTimer - dt
        if selectedTimer <= 0 then
            selectedTimer = 0
        end
    end

    if (input.check("leftBump") or input.check("rightBump")) and selectedTimer == 0 then
        local dir = 0
        if input.check("rightBump") then
            dir = 1
        else
            dir = -1
        end
        selected = selected + dir
        if selected == 5 then
            selected = selected + dir
        end
        if selected <= 0 then
            selected = 6
        elseif selected >= 7 then
            selected = 1
        end
        while not power.check(selected) do
            selected = selected + dir
            if selected <= 0 then
                    selected = 6
            elseif selected >= 7 then
                selected = 1
            end
        end
        selectedTimer = 0.1
    end

    power.kill()

end

-- Creating new powers
function power.create(dt, element)
    if world.current ~= "overworld" then
        timer = timer + dt
        local dmgMultiplier = 1
        if timer >= 0.1 and power.delay == 0 then
            power.delay = 0.5
            if element == "waterShot" then
                dmgMultiplier = 1
                sfx.shotWater:play()
            elseif element == "fireBreath" then
                dmgMultiplier = 2
                sfx.shotFire:stop()
                sfx.shotFire:play()
            elseif element == "airShove" then
                dmgMultiplier = 0.5
                sfx.shotAir:stop()
                sfx.shotAir:play()
            elseif element == "wildNature" then
                dmgMultiplier = 0.1
                sfx.shotGrass:play()
            end
            player.projectile[#player.projectile + 1] = {
                x = player.x + ((player.w) / 2) + (player.w / 2 * player.xDir) - 8,
                y = player.y + ((player.h) / 2) + (player.h / 2 * player.yDir) - 8,
                image = element,
                -- projectile accomodating 8 direction shooting
                xDir  = player.xDir,
                yDir  = player.yDir,
                dmg   = dmgMultiplier,
                w = 16,
                h = 16
            }
            timer = timer - 0.1
        end
    end
end


-- Drawing the projectiles
function power.draw()
    for key, projectile in ipairs(player.projectile) do
        -- Find angle of projectile
        local angle = math.atan2(projectile.yDir, projectile.xDir)

        -- Draw that projectile
        local dist = math.dist(projectile.x, projectile.y, player.x, player.y)
        local alpha = 255 - dist
        g.setColor(255, 255, 255, alpha)
        g.draw(gfx.projectile[projectile.image], projectile.x + 8, projectile.y + 8, angle, 1, 1, 8, 8)

    end
    for key, minion in ipairs(enemies) do
        if minion.stuck and not minion.boss then
            g.setColor(255, 255, 255)
            g.draw(gfx.projectile.root, minion.x + (minion.w - 16) / 2, minion.y - 16 + minion.h)
        end
    end
end


-- Check collision with objects and destroy
function power.kill()
    for key, projectile in ipairs(player.projectile) do
        if math.overlapTable(collisions, projectile) then
            table.remove(player.projectile, key)
        end
    end

    for key, projectile in ipairs(player.projectile) do
        if math.dist(projectile.x, projectile.y, player.x,player.y) > 255 then
            table.remove(player.projectile, key)
        end
    end

    -- Check collision with enemies and damage them
    for key, projectile in ipairs(player.projectile) do
        local dmgMulti = projectile.dmg
        for keys, minion in ipairs(enemies) do
            if math.overlap(projectile, minion) then
                if minion.boss and minion.stuck then
                    if minion.boss then
                        if world.current ~= "hell" then
                            minion.hurt = true
                        else
                            if minion.type == "water" then
                                if projectile.image == "airShove" then
                                    minion.type = "fire"
                                    minion.hurt = true
                                    table.remove(player.projectile, key)
                                end
                            elseif minion.type == "fire" then
                                if projectile.image == "waterShot" then
                                    minion.type = "grass"
                                    minion.hurt = true
                                    table.remove(player.projectile, key)
                                end
                            elseif minion.type == "grass" then
                                if projectile.image == "fireBreath" then
                                    minion.type = "air"
                                    minion.hurt = true
                                    table.remove(player.projectile, key)
                                end
                            elseif minion.type == "air" then
                                if projectile.image == "wildNature" then
                                    minion.type = "dust"
                                    minion.hurt = true
                                    vulDir = 1
                                    table.remove(player.projectile, key)
                                end
                            end
                        end
                    end
                end
                if minion.name == "broodmother" then
                    if minion.type == "dust" and not minion.hurt and vul > 1 then
                        chat.start(txt.hellBossDefeat)
                        table.remove(enemies, keys)
                    end
                    table.remove(player.projectile, key)
                end
                if projectile.image == "fireBreath" then
                    if minion.stuck and not minion.boss then
                        minion.stuck = false
                        projectile.dmg = projectile.dmg + 1
                    elseif minion.boss then
                        if minion.type == "grass" then
                            dmgMulti = dmgMulti * 1.5
                        end
                    end
                elseif projectile.image == "waterShot" then
                    minion.shove = 2
                    if minion.boss then
                        if minion.type == "fire" then
                            dmgMulti = dmgMulti * 2
                        end
                    end
                elseif projectile.image == "airShove" then
                    minion.shove = 4
                            if minion.boss then
                                if minion.type == "dust" then
                                    dmgMulti = dmgMulti * 6
                                end
                            end
                elseif projectile.image == "wildNature" then
                    if not minion.boss then
                        minion.stuck = true
                        minion.rootTimer = 10
                    else
                        if minion.type == "air" then
                            dmgMulti = dmgMulti * 30
                        end
                    end
                end
                if not (minion.boss and not minion.stuck) then
                    minion.hp = minion.hp - (10 * dmgMulti / countOpenWorlds())
                end
            end
            if math.overlap(projectile, minion) then
                table.remove(player.projectile, key)
            end
        end
    end

end


-- Code for whirlwind
defence = { }

defenceTimer = 0

function defence.update(dt)

    if input.check("back") and not defenceDelay and map.current.name ~= "overworld" then
        defenceTimer = defenceTimer + dt
        if defenceTimer >= 1 then
            defenceDelay = true
        end

    elseif defenceTimer > 0 then
        defenceTimer = defenceTimer - dt
        if defenceTimer <= 0 then
            defenceTimer = 0
            defenceDelay = false
        end
    end

end


function defence.draw()

    --g.setColor(139,69,19)
    --g.circle("fill", player.x + player.w / 2, player.y + player.h / 2, player.w / 2, 32)
    g.setColor(255,255,255)
    g.draw(gfx.projectile.whirlwind, player.x + player.w / 2, player.y + player.h / 2, math.rad(defenceTimer * 960), 1, 1, gfx.projectile.whirlwind:getWidth() / 2, gfx.projectile.whirlwind:getHeight() / 2)

end


-- Disabling powers at start and making sure they are able to be enabled

function power.useable(element)
    local available = false
    if power[element] then
        if player[element].delay then
            available = true
        end
    end
    return available
end


function power.check(number)

    -- Value to return
    local returnValue = false

    if number == 1 then
        returnValue = power.waterShot
    elseif number == 2 then
        returnValue = power.fireBreath
    elseif number == 3 then
        returnValue = power.airShove
    elseif number == 4 then
        returnValue = power.wildNature
    elseif number == 5 then
        returnValue = power.defence
    else
        returnValue = true
    end

    return returnValue

end



-- Power overlay information power.delay = 0.5 at start
function power.overlay()

    local name = nil
    if selected == 1 then
        name = "waterShot"
    elseif selected == 2 then
        name = "fireBreath"
    elseif selected == 3 then
        name = "airShove"
    elseif selected == 4 then
        name = "wildNature"
    end
    if selected ~= 5 then
        g.setColor(128 + (127 * (2 * power.delay)), 128 - (128 * (2 * power.delay)), 128 - ( 128 * (2 * power.delay)))
    g.circle("fill", 24, 24, 20 - (15 * (2 * power.delay)), 32)
    end
    if name then
        g.setColor(255, 255, 255)
        g.draw(gfx.projectile[name], 16, 16)
    elseif selected == 5 then
        g.setColor(128 + (127 * defenceTimer), 128 - (128 * defenceTimer), 128- (128 * defenceTimer))
        g.circle("fill", 24, 24, 20 - (12 * defenceTimer), 32)
        g.setColor(128, 64, 32)
        g.circle("fill", 24, 24, 8, 32)
    end


end
